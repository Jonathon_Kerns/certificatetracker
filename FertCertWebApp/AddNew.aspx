﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="FertCertWebApp.AddNew" MasterPageFile="~/Site.Master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Adding Companies and Employees</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

    <nav>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="comp-tab" data-toggle="tab" href="#comp" role="tab" aria-controls="comp" aria-selected="true">New Company/Branch</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="emp-tab" data-toggle="tab" href="#emp" role="tab" aria-controls="emp" aria-selected="false">New Employee</a>
            </li>
        </ul>
    </nav>
    <div class="tab-content" id="myTabContent">
        <div class="container tab-pane fade show active cup-border" id="comp" role="tabpanel" aria-labelledby="comp-tab">
            <%--<asp:Panel ID="EmpPanel_NewComp" runat="server" CssClass="form_panel" Visible="true">--%>
            <br />
            <br />
                <h3>Add New Company or Branch</h3>
                <p class="instructions">Use the form below to add a company to the database. 
                    For a list of the companies currently tracked by our database, please see the <a href="\Directory">View Companies</a> page.</p>
                <p class="instructions">Items marked with an asterisk (<span class="fa fa-asterisk fa-lg"></span>) are required fields.</p>
                <hr />
                <br />

                <div class="row">
                    <div class="col-md-12">
                        
                        <p class="instructions"><span class="fa fa-asterisk fa-lg pr-1"></span>Select an option below to start:</p>
                    </div>
                </div>

                <%--<div class="row btn-group" role="group" aria-label="radios">
                    <div class="col-md-4 box_me">
                        <p class="form_label"><asp:RadioButton ID="CompRadio_Comp" AutoPostBack="true" runat="server" Checked="true" GroupName="CompRadioGroup" CssClass="radio_select" TabIndex="1" OnCheckedChanged="CompRadio_Comp_CheckedChanged"/>  New Company</p>
                        <p class="hint_text">Select this option if you need to create a new company with a single branch.</p>
                    </div>
                    <div class="col-md-4 box_me">
                        <p class="form_label"><asp:RadioButton ID="CompRadio_CompBranch" AutoPostBack="true" runat="server" Checked="false" GroupName="CompRadioGroup" CssClass="radio_select" TabIndex="2" OnCheckedChanged="CompRadio_CompBranch_CheckedChanged"/>  New Branch of New Company</p>
                        <p class="hint_text">Select this option if you need to create a new company that has multiple branches.</p>
                    </div>
                    <div class="col-md-4 box_me">
                        <p class="form_label"><asp:RadioButton ID="CompRadio_Branch" AutoPostBack="true" runat="server" Checked="false" GroupName="CompRadioGroup" CssClass="radio_select" TabIndex="3" OnCheckedChanged="CompRadio_Branch_CheckedChanged"/>  New Branch of Existing Company</p>
                        <p class="hint_text">Select this option if you need to add an additional branch to a company that already exists in the database.</p>
                    </div>
                </div>--%>
                
                
                <div class="card-deck" data-toggle="buttons">
                    <div class="row h-100">
                        <div class="col-md-6">
                              <div class="card h-100">
                                    <div class="card-header h-25" onclick="clickRadio(true)">
                                        <label class="btn active radSpan">
                                                <%--<input type="radio" name="options" id="option1" checked> --%>
                                                <%--<span class="fa fa-check fa-lg pull-left pl-1"></span>--%>
                                                <%--<h4 class="radSpan"><span class="fa fa-check-circle fa-lg fa-pull-left"></span>New Company</h4>--%>
                                            <input type="radio" name="options" id="option1" checked>
                                            <span class="fa fa-check-circle fa-lg fa-pull-left"></span>New Company
                                        </label>
                                    </div>

                                    <div class="card-body my-card" >
                                        <div>
                                            <h5 class="card-title">Create a new company.</h5>
                                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                        </div>
                                    </div>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="card h-100">
                                    <div class="card-header h-25"  onclick="clickRadio(false)">
                                        <label class="btn radSpan">
                                            <input type="radio" name="options" id="option2">                                             
                                            <span class="fa fa-check-circle fa-lg fa-pull-left"></span>New Branch of Existing Company
                                        </label>
                                    </div>

                                    <div class="card-body my-card" >
                                        <div>
                                            <h5 class="card-title">Create a new branch (company already exists in database).</h5>
                                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                        </div>
                                    </div>
                              </div>
                        </div>
                    </div>
                </div><%--div class="card-deck"--%>
                
                <div class="tiny-me">
                    <div class="row btn-group" role="group" aria-label="radios">
                        <div class="col-md-4 box_me">
                            <%--<p class="form_label"><asp:RadioButton ID="CompRadio_Comp" AutoPostBack="true" runat="server" Checked="true" GroupName="CompRadioGroup" CssClass="radio_select" TabIndex="1"/></p>
                            --%>
                        </div>
                        <div class="col-md-4 box_me">
                            <%--<p class="form_label"><asp:RadioButton ID="CompRadio_CompBranch" AutoPostBack="true" runat="server" Checked="false" GroupName="CompRadioGroup" CssClass="radio_select" TabIndex="2" /></p>
                            --%>
                        </div>
                        <div class="col-md-4 box_me">
                            <p class="form_label"><asp:RadioButton ID="CompRadio_Branch" runat="server" Checked="false" GroupName="CompRadioGroup" CssClass="radio_select" TabIndex="3" /></p>                            
                        </div>
                    </div>
                </div> <%--<div class="tiny-me">--%>
                <br />

                <div class="row">
                    <button type="button" class="open-collapsible" ><span class="fa fa-folder-open fa-lg pr-2"></span>   Main Company Details<span class="fa fa-asterisk fa-lg pull-right"></span><span class="fa fa-asterisk fa-lg pull-right"></span></button>
                </div>
                <br />
                <div id="main_company_details">
                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Company Name:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompNameBox"
                                AutoPostBack="False"
                                CssClass="form_box"
                                MaxLength="500"
                                TabIndex="4"
                                Width="100%"
                                ToolTip="Enter the company name.">
                            </asp:TextBox>
                            <asp:DropDownList
                                runat="server" 
                                ID="CompDropDown_forBranch"
                                CssClass="form_list"
                                AutoPostBack="False"
                                AppendDataBoundItems="true"
                                TabIndex="5"
                                Width="100%"
                                ToolTip="Select the company you want to create a new branch for. If you do not find the company in this list, please select the NEW COMPANY Radio Button."
                                
                                >
                            </asp:DropDownList>
                            <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Company Branch Name:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompBranchBox"
                                CssClass="form_box"
                                MaxLength="500"
                                TabIndex="6"
                                Width="100%"
                                ToolTip="Enter the company branch name."
                                AutoPostBack="False"
                                Text="Main Branch">
                            </asp:TextBox>
                            <span class="fa fa-asterisk fa-lg" id="BranchRequired" runat="server"></span> <%--will runat server work?--%>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <button type="button" class="open-collapsible" data-toggle="collapse" data-target="#company_address"><span class="fa fa-map-marker fa-lg pr-4"></span>   Company Address</button>
                </div>
                <br />
                <div class="collapse" id="company_address" >
                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Company Address (Line One):</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompAddrBox1"
                                CssClass="form_box"
                                MaxLength="255"
                                TabIndex="7"
                                Width="100%"
                                ToolTip="Enter the company/company branch street address (1 of 2).">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Company Address (Line Two):</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompAddrBox2"
                                CssClass="form_box"
                                MaxLength="255"
                                TabIndex="8"
                                Width="100%"
                                ToolTip="Enter the company/ company branch street address (2 of 2).">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">City:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompCityBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="9"
                                Width="100%"
                                ToolTip="Enter the comapny's city.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">State:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompStateBox"
                                CssClass="form_box"
                                MaxLength="2"
                                TabIndex="10"
                                Width="100%"
                                ToolTip="Enter the company's state.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Zip Code:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompZipBox1"
                                CssClass="form_box"
                                MaxLength="5"
                                TabIndex="11"
                                TextMode="Number"
                                Width="100%"
                                ToolTip="Enter the company's zip code (5 digits).">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Zip Code Extension:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompZipBox2"
                                CssClass="form_box"
                                MaxLength="4"
                                TabIndex="12"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the company's zip code extension (4 digits).">
                            </asp:TextBox>
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <button type="button" class="open-collapsible" data-toggle="collapse" data-target="#company_contact"><span class="fa fa-address-card fa-lg pr-2"></span>   Primary Company Contact</button>
                </div>
                <br />
                <div class="collapse" id="company_contact" >
                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Primary Company Contact First Name:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompPriContFirstBox"
                                CssClass="form_box"
                                MaxLength="320"
                                TabIndex="13"
                                Width="100%"
                                ToolTip="Enter the company primary contact's first name.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Primary Company Contact Last Name:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompPriContLastBox"
                                CssClass="form_box"
                                MaxLength="320"
                                TabIndex="14"
                                Width="100%"
                                ToolTip="Enter the company primary contact's last name.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Primary Contact Email:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompPriContEmailBox"
                                CssClass="form_box"
                                MaxLength="320"
                                TabIndex="15"
                                Width="100%"
                                TextMode="Email"
                                ToolTip="Enter the company contact's email address.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Primary Contact Phone:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompPriContPhoneBox"
                                CssClass="form_box"
                                MaxLength="10"
                                TabIndex="16"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the company contact's 10-digit phone number.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Primary Contact Phone Extension:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CompPriContPhoneExtBox"
                                CssClass="form_box"
                                MaxLength="10"
                                TabIndex="17"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the company contact's phone number extension.">
                            </asp:TextBox>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <p>After reviewing your entries above, click [Save New Company] below to commit this entry to the database.</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Button 
                            runat="server" 
                            ID="SaveNewCompButton" 
                            Text="Save New Company Details"
                            CssClass="save_button"
                            TabIndex="18"
                            ToolTip="Click here to validate the details above and save your new company information to the database."
                            OnClick="SaveNewCompButton_Click"
                            ></asp:Button> 
                    </div>
                </div>
            <%--</asp:Panel>--%><%--EmpPanel_NewComp--%>
        </div>
        <div class="container tab-pane fade cup-border" id="emp" role="tabpanel" aria-labelledby="emp-tab">
            <br />
            <br />
            <%--<asp:Panel ID="EmpPanel_NewEmp" runat="server" CssClass="form_panel" Visible="true">--%>
                <h3>Add New Employee</h3>
                <p class="instructions">Use the form below to add an employee to the database. 
                    If the employee is from a company not yet tracked by this program, please first add that company via the tab. 
                    For a list of the companies currently tracked by our database, please see the <a href="\Directory">View Companies</a> page.</p>
                <p class="instructions">Items marked with an asterisk (<span class="fa fa-asterisk fa-lg"></span>) are required fields.</p>
                <hr />
                <br />
                <%--<asp:Button runat="server" ID="testButton" OnClick="testButton_Click" Visible="false" />--%>

                <div class="row">
                    <button type="button" class="open-collapsible"><span class="fa fa-user fa-lg pr-2"></span>   Main Employee Details<span class="fa fa-asterisk fa-lg pull-right"></span><span class="fa fa-asterisk fa-lg pull-right"></span></button>
                </div>
                <br />
                <div id="main_emp_details">

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">First Name:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="FirstNameBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="1"
                                Width="100%"
                                ToolTip="Enter the employee's first name.">
                            </asp:TextBox>
                            <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Middle Initial:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="MiddleInitBox"
                                CssClass="form_box"
                                MaxLength="1"
                                TabIndex="2"
                                Width="100%"
                                ToolTip="Enter the employee's middle initial.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Last Name:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="LastNameBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="3"
                                Width="100%"
                                ToolTip="Enter the employee's last name.">
                            </asp:TextBox>
                            <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Email:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="EmpEmailBox"
                                CssClass="form_box"
                                MaxLength="320"
                                TabIndex="5"
                                Width="100%"
                                TextMode="Email"
                                ToolTip="Enter the employee's email address.">
                            </asp:TextBox>
                           
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Company:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:DropDownList
                                runat="server" 
                                ID="CompanyDropList"
                                CssClass="form_list"
                                AutoPostBack="False"
                    
                                TabIndex="14"
                                Width="100%"
                                ToolTip="Select the comapny the employee currently works for. If you do not find it in this list, please create the company first."
                                >
                      
                            </asp:DropDownList>
                            <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Employment Start Date:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="EmpStartDateBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="17"
                                Width="100%"
                                TextMode="Date"
                                ToolTip="Enter the employee's start date at current company (choose TODAY if unknown).">
                            </asp:TextBox>
                            <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>
                </div>
   
                <div class="row">
                    <button type="button" class="open-collapsible" data-toggle="collapse" data-target="#emp_contact_details"><span class="fa fa-address-card fa-lg pr-2"></span>   Employee Supplemental and Address Info</button>
                </div>
                <br />
                <div class="collapse" id="emp_contact_details">

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Date of Birth:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="BirthBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="4"
                                Width="100%"
                                TextMode="Date"
                                ToolTip="Enter the employee's birthday.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Phone:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="PhoneBox"
                                CssClass="form_box"
                                MaxLength="10"
                                TabIndex="6"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the employee's 10-digit phone number.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Phone Extension:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="PhoneExtBox"
                                CssClass="form_box"
                                MaxLength="10"
                                TabIndex="7"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the employee's phone number extension.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Personal Address (Line One):</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="Address1Box"
                                CssClass="form_box"
                                MaxLength="255"
                                TabIndex="8"
                                Width="100%"
                                ToolTip="Enter the employee's personal street address (1 of 2).">
                            </asp:TextBox>
                             <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Personal Address (Line Two):</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="Address2Box"
                                CssClass="form_box"
                                MaxLength="255"
                                TabIndex="9"
                                Width="100%"
                                ToolTip="Enter the employee's personal street address (2 of 2).">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">City:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="CityBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="10"
                                Width="100%"
                                ToolTip="Enter the employee's city.">
                            </asp:TextBox>
                             <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">State:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="StateBox"
                                CssClass="form_box"
                                MaxLength="2"
                                TabIndex="11"
                                Width="100%"
                                ToolTip="Enter the employee's state.">
                            </asp:TextBox>
                             <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Zip Code:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="Zip1Box"
                                CssClass="form_box"
                                MaxLength="5"
                                TabIndex="12"
                                TextMode="Number"
                                Width="100%"
                                ToolTip="Enter the employee's zip code (5 digits).">
                            </asp:TextBox>
                             <span class="fa fa-asterisk fa-lg"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Zip Code Extension:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="Zip2Box"
                                CssClass="form_box"
                                MaxLength="4"
                                TabIndex="13"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the employee's zip code extension (4 digits).">
                            </asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <button type="button" class="open-collapsible" data-toggle="collapse" data-target="#emp_comp_details"><span class="fa fa-folder-open fa-lg pr-2"></span>  Employee Supplemental Company Info</button>
                </div>
                <br />
                <div class="collapse" id="emp_comp_details">
        
                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Employee Job Title:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="EmpJobTitleBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="15"
                                Width="100%"
                                ToolTip="Enter the employee's job title.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Company Employee ID:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="EmpCompanyIDBox"
                                CssClass="form_box"
                                MaxLength="50"
                                TabIndex="16"
                                Width="100%"
                                ToolTip="Enter the employee's identification number used by the company.">
                            </asp:TextBox>
                        </div>
                    </div>
        
                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Supervisor's Name:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="SupNameBox"
                                CssClass="form_box"
                                MaxLength="255"
                                TabIndex="18"
                                Width="100%"
                                ToolTip="Enter the employee's supervisor's name.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Supervisor's Email:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="SupEmailBox"
                                CssClass="form_box"
                                MaxLength="320"
                                TabIndex="19"
                                Width="100%"
                                TextMode="Email"
                                ToolTip="Enter the supervisor's email address.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Supervisor's Phone:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="SupPhoneBox"
                                CssClass="form_box"
                                MaxLength="10"
                                TabIndex="20"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the supervisor's 10-digit phone number.">
                            </asp:TextBox>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="form_label">Supervisor's Phone Extension:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:TextBox 
                                runat="server" 
                                ID="SupPhoneExtBox"
                                CssClass="form_box"
                                MaxLength="10"
                                TabIndex="21"
                                Width="100%"
                                TextMode="Number"
                                ToolTip="Enter the supervisor's phone number extension.">
                            </asp:TextBox>
                        </div>
                    </div>
                </div>
                
                <hr />
                <div class="row">
                    <div class="col-md-12">
                        <%--<p class="instructions">After reviewing your entries above, click "Save New Employee" below to commit this entry to the database.</p>--%>
                        <asp:Label runat="server" CssClass="instructions" ID="EmpSaveWarning" Text="After reviewing your entries above, click [Save New Employee] below to commit this entry to the database." ></asp:Label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <asp:Button 
                            runat="server" 
                            ID="SaveNewEmpButton" 
                            Text="Save New Employee"
                            CssClass="save_button"
                            TabIndex="22"
                            ToolTip="Click here to validate the details above and save your new employee to the database."
                            OnClick="SaveNewEmpButton_Click"
                            />
                    </div>
                </div>
        </div>
    </div> <%--id="myTabContent"--%>

    <div class="modal fade" id="modalCompWarning" tabindex="-1" role="dialog" aria-labelledby="modalCompWarningLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header alert-primary" style="background-color: goldenrod; color: black;">
                    <h4 class="modal-title text-center" id="modalCompWarningLabel"><span class="fa fa-exclamation-triangle fa-lg pr-2"></span> WARNING, CHANGES NOT SAVED! <span class="fa fa-exclamation-triangle fa-lg pl-2"></span></h4>
                </div>
                <div class="modal-body">
                    <h4>ISSUES DETECTED WITH MANDATORY FIELDS.</h4>
                    <p>Your changes were not saved to the database. To continue, correct the inputs listed below.</p>
                    <hr />
                    <asp:Label runat="server" CssClass="instructions" ID="CompSaveWarning" Text=""></asp:Label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: goldenrod; color: black;">OK</button>
                </div>
            </div>
        </div>
    </div> <%--id="modalCompWarning"--%>

    <div class="modal fade" id="modalCompError" tabindex="-1" role="dialog" aria-labelledby="modalCompErrorLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header alert-primary" style="background-color: firebrick; color: white;">
                    <h4 class="modal-title text-center" id="modalCompErrorLabel"><span class="fa fa-times fa-lg pr-2"></span>ERROR, CHANGES NOT SAVED! <span class="fa fa-times fa-lg pl-2"></span></h4>
                </div>
                <div class="modal-body">
                    <h4>AN ERROR HAS OCCURRED. NO UPDATES WERE MADE.</h4>
                    <p>Your changes were NOT saved to the database. Please review the details below.</p>
                    <hr />

                    <h4 class="error_label">Error Summary: </h4>
                    <asp:Label runat="server" ID="CompErrorLabel" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."></asp:Label>
                    <h4 class="error_label">Hint: </h4>
                    <asp:Label runat="server" ID="CompHintLabel" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."></asp:Label>

                    <hr />
                    <h4>Try to save your changes again.</h4>
                    <p>If you are repeatedly receiving this error, please make a note of the details of this error and contact a developer for assistance.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: firebrick; color: white;">CLOSE</button>
                </div>
            </div>
        </div>
    </div> <%--id="modalCompError"--%>

    <div class="modal fade" id="modalCompSuccess" tabindex="-1" role="dialog" aria-labelledby="modalCompSuccessLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header alert-primary" style="background-color: limegreen; color: black;">
                    <h4 class="modal-title text-center" id="modalCompSuccessLabel"><span class="fa fa-check-circle"></span>SUCCESS! <span class="fa fa-check-circle"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <h4>CHANGES SAVED.</h4>
                        <p>Your changes have been saved to the database successfully. Please review the details below.</p>
                        <hr />

                        <div class="row grey">
                            <div class="col-md-2 left-border">
                                <p class="success_label">Company Name: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_CompName" Text="The Name Here" CssClass="review_label"></asp:Label>
                            </div>
                            <div class="col-md-2 left-border">
                                <p class="success_label">Branch: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_BranchName" Text="The Branch Here" CssClass="review_label"></asp:Label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2 left-border">
                                <p class="success_label">Company Address: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_CompAddr" Text="The Address Here" CssClass="review_label"></asp:Label>
                            </div>
                            <div class="col-md-2 left-border">
                                <p class="success_label">Primary Contact Name: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_CompPriContact" Text="The Primary Contact Name Here" CssClass="review_label"></asp:Label>
                            </div>
                        </div>

                        <div class="row grey">
                            <div class="col-md-2 left-border">
                                <p class="success_label">Primary Contact Email: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_CompEmail" Text="The Email Address Here" CssClass="review_label"></asp:Label>
                            </div>
                            <div class="col-md-2 left-border">
                                <p class="success_label">Primary Contact Phone: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_CompPriPhone" Text="Primary Contact Phone Here" CssClass="review_label"></asp:Label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2 left-border">
                                <p class="success_label">Created By: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_CompCreateUser" Text="Created By User Here" CssClass="review_label"></asp:Label>
                            </div>
                            <div class="col-md-2 left-border">
                                <p class="success_label">Date Created: </p>
                            </div>
                            <div class="col-md-4 right-border">
                                <asp:Label runat="server" ID="SuccessLabel_CompCreateDate" Text="Date Created Here" CssClass="review_label"></asp:Label>
                            </div>
                        </div>

                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <p class="instructions">If, upon reviewing this entry, you discover a mistake has been made:</p>
                                <p class="instructions">Please see the page <a href="">Modifying Company Details</a> to correct the error, or contact a developer with details of the problem and reference the number below.</p>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <p class="instructions">Company Database ID Number:</p>
                            </div>
                            <div class="col-md-8">
                                <asp:Label runat="server" ID="SuccessLabel_CompID" CssClass="review_label" Text="CompanyID Here"></asp:Label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: limegreen; color: black;">OK</button>
                </div>
            </div>
        </div>
    </div> <%--id="modalCompSuccess"--%>

    <div class="modal fade" id="modalEmpSuccess" tabindex="-1" role="dialog" aria-labelledby="modalEmpSuccessLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header alert-primary" style="background-color: firebrick; color: white;">
                    <div class="modal-header alert-primary" style="background-color: limegreen; color: black;">
                        <h4 class="modal-title text-center" id="modalEmpSuccessLabel"><span class="fa fa-check-circle fa-lg pr-2"></span>SUCCESS! <span class="fa fa-check-circle fa-lg pl-2"></span></h4>
                    </div>
                </div>
                <div class="modal-body">
                    <h4>CHANGES SAVED.</h4>
                    <p>Your changes have been saved to the database successfully. Please review the details below.</p>
                    <hr />

                    <div class="row">
                        <div class="col-md-2">
                            <p class="success_label">Employee Name: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpName" Text="The Name Here" CssClass="review_label"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <p class="success_label">DOB: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpDOB" Text="The Date Here" CssClass="review_label"></asp:Label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <p class="success_label">Email: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpEmail" Text="The Email Here" CssClass="review_label"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <p class="success_label">Company: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpComp" Text="The Company Here" CssClass="review_label"></asp:Label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <p class="success_label">Phone: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpPhone" Text="Phone Number Here" CssClass="review_label"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <p class="success_label">Address: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpAddr" Text="Employee Address Here" CssClass="review_label"></asp:Label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <p class="success_label">Created By: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpCreateUser" Text="Created By User Here" CssClass="review_label"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <p class="success_label">Date Created: </p>
                        </div>
                        <div class="col-md-4">
                            <asp:Label runat="server" ID="SuccessLabel_EmpCreateDate" Text="Date Created Here" CssClass="review_label"></asp:Label>
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-md-12">
                            <p class="instructions">If, upon reviewing this entry, you discover a mistake has been made:</p>
                            <p class="instructions">Please see the page <a href="">Modifying Employee Details</a> to correct the error, or contact a developer with details of the problem and reference the number below.</p>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <p class="instructions">Company Database ID Number:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:Label runat="server" ID="SuccessLabel_EmpID" CssClass="review_label" Text="EmpID Here"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="instructions">Company Database ID Number:</p>
                        </div>
                        <div class="col-md-8">
                            <asp:Label runat="server" ID="SuccessLabel_CompEmpID" CssClass="review_label" Text="CompanyID Here"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: limegreen; color: black;">OK</button>
                </div>
            </div>
        </div>
    </div> <%--id="modalEmpSuccess"--%>

</asp:Content>