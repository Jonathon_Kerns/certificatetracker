﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace FertCertWebApp
{
    public partial class SiteMaster : MasterPage
    {
        override protected void OnInit(EventArgs e)
        {
            ///////////////////////////////
            if (IsUserAuthorized())
            {
                //Server.Transfer("Default.aspx");
            }
            else
            {
                Server.Transfer("Denied.aspx");
            }
            ///////////////////////////////

        }
        private List<string> _authorizedGroupsList = new List<string>()
       {
           //Below are possible user groups, tbh-- cannot tell which of these work so all included:
           
           @"city.talgov.com\ISS_Gis"//, 
         //@"city.talgov.com\ISS_Gis"
       };
        private bool IsUserAuthorized()
        {
            for (int i = 0; i < _authorizedGroupsList.Count; i++)
            {
                try
                {
                    //if (Page.User.IsInRole(_authorizedGroupsList[i]))
                    //{
                    //    return true;
                    //}
                    if (HttpContext.Current.User.IsInRole(_authorizedGroupsList[i]))
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    // not doing anything exceptions right now
                    continue;
                }
            }
            return false;
        }
        //////////////////////////////////////////////////////////////
        //                                                          //
        // Set CurrentMode to be one of                             //
        // the Mode enum types below...                             //
        //                                                          //
        //////////////////////////////////////////////////////////////
        protected internal enum Mode : int
        {
            Debug,  /* local db */
            Test,   /* test db */
            Prod,   /* production db */
            None    /* Do nothing -- will throw error*/
        }
        protected internal static SiteMaster.Mode CurrentMode = Mode.Debug;
        protected internal static string connectionStr;
        //////////////////////////////////////////////////////////////
        //                                                          //
        // ...This affects which connection string                  //
        // is used to connect to the database.                      //
        //                                                          //
        //////////////////////////////////////////////////////////////
        protected void Page_Load(object sender, EventArgs e)
        {
            SetString();
        }
        public class DropDownItem
        {
            public int companyID { get; set; }
            public string companyName { get; set; }
            //public string branchName { get; set; }
        }//class DropDownItem

        protected internal static string SetString()
        {
            string connectionStr = "";// = ConfigurationManager.ConnectionStrings["LocalDemoDatabase"].ConnectionString;

            if (CurrentMode is Mode.Prod) { connectionStr = ConfigurationManager.ConnectionStrings["ProdDatabase"].ConnectionString; }
            else if (CurrentMode is Mode.Debug) { connectionStr = ConfigurationManager.ConnectionStrings["LocalDemoDatabase"].ConnectionString; }
            else if (CurrentMode is Mode.Test) { connectionStr = ConfigurationManager.ConnectionStrings["TestDatabase"].ConnectionString; }
            else { }

            return connectionStr;
        }
        
        protected internal static List<DropDownItem> FillCompanyDropDrown(SqlConnection con)
        {
            List<DropDownItem> listItems = new List<DropDownItem>();

            try
            {
                string query = "SELECT [CompanyID], [CompanyName] + ' (' + [CompanyBranchName] + ')' as 'CompanyName' from [dbo].[Company] ORDER BY [CompanyName] ASC";
                using (con)
                {
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlCommand selectCmd = new SqlCommand(query, con);
                    SqlDataReader readMe;
                    using (readMe = selectCmd.ExecuteReader())
                    {
                        while (readMe.Read())
                        {
                            //string branch = readMe["CompanyBranchName"].ToString();
                            int companyID = (int)readMe["CompanyID"];
                            string companyName = readMe["CompanyName"].ToString();

                            DropDownItem item = new DropDownItem
                            {
                                companyID = companyID,
                                companyName = companyName,
                                //branchName = (!string.IsNullOrEmpty(branch)) ? companyName + " (" + branch + ")" : companyName
                            };
                            listItems.Add(item);
                        }
                    }
                    if (!readMe.IsClosed) readMe.Close();
                }
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception exREAD)
            {
                Console.Write(exREAD.ToString());
            }
            return listItems;
        }//FillCompanyDropDrown
    }
}