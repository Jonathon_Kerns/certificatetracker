﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OldCertificates.aspx.cs" Inherits="FertCertWebApp.OldCertificates" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Managing Certificates</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

    <div class="row" id="pushButtonBar">
        <div class="col-xs-1"></div>

        <div class="col-xs-6">
            <asp:Button ID="ViewCertButton"  
                runat="server"   
                Text="View Recent"          
                CssClass="collapsible selected" 
                ToolTip=""/>
            <asp:Button ID="AddCompButton"  
                runat="server"   
                Text="Add Completed"      
                CssClass="collapsible deselected"  
                ToolTip="" />
            <asp:Button ID="AddCertButton"  
                runat="server"   
                Text="Add New Type"      
                CssClass="collapsible deselected"  
                ToolTip="" />
        </div>

        <div class="col-xs-5"></div>
    </div>

    <asp:Panel ID="CertPanel_NewCert" runat="server" Visible="false"> <%-- Add new employee completed cert --%>

    </asp:Panel>

    <asp:Panel ID="CertPanel_ViewCert" runat="server" Visible="false"> <%-- View recently added certs --%>
        <h3>View Recently Completed Certificates</h3>
        <p>Use the table below to view employee certificates that have been entered into the database.</p>
    </asp:Panel>

    <asp:Panel ID="CertPanel_NewCertType" runat="server" CssClass="form_panel"> <%-- Add new cert type --%>
        <h3>Add New Type of Certificate</h3>
        <p class="instructions">Use the form below to add a new type of certificate to your database.</p>
        <p class="instructions">Items marked with an asterisk (<span class="glyphicon glyphicon-asterisk"></span>) are required fields.</p>
        <hr />
        <br />
        <div class="row">
            <div class="col-md-4">
                <p class="form_label">Certificate Name:</p>
            </div>
            <div class="col-md-8">
                <asp:TextBox 
                    runat="server" 
                    ID="CertNameBox"
                    CssClass="form_box"
                    MaxLength="255"
                    TabIndex="1"
                    Width="100%"
                    ToolTip="Enter the certificate name.">
                </asp:TextBox>
                <span class="glyphicon glyphicon-asterisk"></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <p class="form_label">Certificate Description:</p>
            </div>
            <div class="col-md-8">
                <asp:TextBox 
                    runat="server" 
                    ID="CertDescriptionBox" 
                    CssClass="form_box"
                    MaxLength="500"
                    TextMode="MultiLine"
                    Rows="7"
                    TabIndex="2"
                    Width="100%"
                    ToolTip="Enter the certificate description.">
                </asp:TextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <p class="form_label">Days Certificate is Valid:</p>
            </div>
            <div class="col-md-8">
                <asp:TextBox 
                    runat="server" 
                    ID="CertDaysValidBox" 
                    CssClass="form_box"
                    TabIndex="3"
                    Width="100%"
                    ToolTip="Enter the number of days a certificate is valid after being issued. For example, certificates that are valid for one year will be valid for 365 days."
                    TextMode="Number">
                </asp:TextBox>
                <span class="glyphicon glyphicon-asterisk"></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>After reviewing your entries above, click "Save New Certificate Type" below to commit this entry to the database.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:Button 
                    runat="server" 
                    ID="SaveNewCertButton" 
                    Text="Save New Certificate Type"
                    CssClass="save_button"
                    TabIndex="4"
                    ToolTip="Click here to validate the details above and save your certificate to the database."
                    />
                </div>
        </div>


        <br />
        <asp:Panel runat="server" ID="ErrorPanel_NewCertType" CssClass="error_panel">
            <p><span class="glyphicon glyphicon-alert"></span>    AN ERROR HAS OCCURRED    <span class="glyphicon glyphicon-alert"></span></p>
            <p>Your changes were not saved to the database. Please review the details below and contact a developer if the error persists.</p>
            <hr />

            <p class="error_label">Error Summary: </p>
            <asp:Label runat="server" ID="ErrorLabel" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."></asp:Label>
            <p class="error_label">Hint: </p>
            <asp:Label runat="server" ID="HintLabel" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."></asp:Label>
        </asp:Panel>

        <asp:Panel runat="server" ID="SuccessPanel_NewCertType" CssClass="success_panel">
            <p><span class="glyphicon glyphicon-ok-sign"></span>  SUCCESS  <span class="glyphicon glyphicon-ok-sign"></span></p>
            <p>Your changes have been saved to the database. Please review the details below.</p>
            <hr />

            <div class="row">
                <div class="col-md-2">
                   <p class="success_label">New Certificate Name: </p>
                </div>
                <div class="col-md-4">
                    <asp:Label runat="server" ID="SuccessLabel_CertName" Text="Certificate Name Here" CssClass="review_label"></asp:Label>
                </div>
                <div class="col-md-2">
                   <p class="success_label">Days Valid: </p>
                </div>
                <div class="col-md-4">
                    <asp:Label runat="server" ID="SuccessLabel_CertDaysValid" Text="Days Valid Here" CssClass="review_label"></asp:Label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                   <p class="success_label">Description: </p>
                </div>
                <div class="col-md-10">
                    <asp:Label runat="server" ID="SuccessLabel_CertDesc" CssClass="review_label" Text="Cerificate Description Here: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."></asp:Label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                   <p class="success_label">Created By: </p>
                </div>
                <div class="col-md-4">
                    <asp:Label runat="server" ID="SuccessLabel_CertUser" Text="Created By User Here" CssClass="review_label"></asp:Label>
                </div>
                <div class="col-md-2">
                   <p class="success_label">Date Created: </p>
                </div>
                <div class="col-md-4">
                    <asp:Label runat="server" ID="SuccessLabel_CertDate" Text="Date Created Here" CssClass="review_label"></asp:Label>
                </div>
            </div>
            
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <p>If, upon reviewing this entry, you discover a mistake has been made:</p>
                    <p>Please see the page <a href="">Modifying Certificate Details</a> to correct the error,</p>
                    <p>Or contact a developer with details of the problem and reference Certificate Database ID Number:</p>
                    <asp:Label runat="server" ID="SuccessLabel_CertID" Text="CertificateID Here"></asp:Label>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>

</asp:Content>
