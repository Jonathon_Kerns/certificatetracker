﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FertCertWebApp
{
    public partial class CertificateTracker : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con;
            if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
            //else { con = new SqlConnection(SiteMaster.connectionStr); }
            else { con = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalDemoDatabase"].ConnectionString); }

            if (!Page.IsPostBack)
            {
                FillDropDown(con);
                datePicker.SelectedDate = DateTime.Today;
            }
            else
            {
                //this.DataBind();
            }
        }
        public class DropDownItem
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        private void FillDropDown(SqlConnection con)
        {
            List<DropDownItem> ddlist = new List<DropDownItem>();
            DropDownItem deflt = new DropDownItem
            {
                ID = 0,
                Name = "Select Employee"
            };
            ddlist.Add(deflt);
            try
            {
                string query = "Select e.EmployeeID, e.FirstName, e.LastName from [dbo].[Employee] e join [dbo].[CompanyEmployee] ce on e.EmployeeID = ce.EmployeeID where ce.ActiveRecord = 1 ORDER BY e.LastName, e.FirstName";
                SqlCommand selectCmd = new SqlCommand(query, con);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (SqlDataReader readMe = selectCmd.ExecuteReader())
                {
                    while (readMe.Read())
                    {
                        string i = readMe["EmployeeID"].ToString();
                        string fn = readMe["FirstName"].ToString();
                        string ln = readMe["LastName"].ToString();

                        DropDownItem item = new DropDownItem
                        {
                            ID = Convert.ToInt32(i),
                            Name = fn + ", " + ln
                        };
                        ddlist.Add(item);
                    }
                }
                if (con.State == ConnectionState.Open)
                    con.Close();
                Emp_DropDownList.DataSource = ddlist;
                Emp_DropDownList.DataTextField = "Name";
                Emp_DropDownList.DataValueField = "ID";
                Emp_DropDownList.DataBind();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            ddlist = new List<DropDownItem>();
            deflt = new DropDownItem
            {
                ID = 0,
                Name = "Select Certificate"
            };
            ddlist.Add(deflt);
            try
            {
                string query = "Select [CertificateID], [CertificateName] from [dbo].[TheCertificate]  ORDER BY  [CertificateID]";
                SqlCommand selectCmd = new SqlCommand(query, con);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (SqlDataReader readMe = selectCmd.ExecuteReader())
                {
                    while (readMe.Read())
                    {
                        string i = readMe["CertificateID"].ToString();
                        string cn = readMe["CertificateName"].ToString();

                        DropDownItem item = new DropDownItem
                        {
                            ID = Convert.ToInt32(i),
                            Name = cn
                        };
                        ddlist.Add(item);
                    }
                }
                if (con.State == ConnectionState.Open)
                    con.Close();
                Cert_DropDownList.DataSource = ddlist;
                Cert_DropDownList.DataTextField = "Name";
                Cert_DropDownList.DataValueField = "ID";
                Cert_DropDownList.DataBind();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }
        protected void Emp_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void Cert_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void SaveNewCertButton_Click(object sender, EventArgs e)
        {
            // TEST FOR EMPTY
            bool cert,em,c;
            cert = String.IsNullOrWhiteSpace(CertNumBox.Text);
            em = Emp_DropDownList.SelectedIndex == 0;
            c = Cert_DropDownList.SelectedIndex == 0;

            string spanB = "<li><span class=\"important\"><h4 class=\"text-danger\">";
            string spanE = "</h4></span></li>";
            if (cert)
            {
                string warningLabel = "<h4>There are values missing from your input. Please enter: </h4><ul>";
                if (c) warningLabel += spanB + "Certificate ID" + spanE;
                if (em) warningLabel += spanB + "Employee Name" + spanE;
                if (cert) warningLabel += spanB + "Certificate Number" + spanE;

                warningLabel += "</ul><h4>Before saving the new certification to the database!</h4>";
                SaveWarning.Text = warningLabel;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalWarning');", true);
                return;
            }

            //PREP FOR SAVING TO DB
            int empID = Emp_DropDownList.SelectedIndex;
            int certID = Cert_DropDownList.SelectedIndex;
            string certDate = datePicker.SelectedDate.ToShortDateString();
            string appID = ApplicatorBox.Text;
            string certNum = CertNumBox.Text;
            string comm = CommentBox.Text;
            string logUser = HttpContext.Current.User.Identity.Name.ToString();
            string today = DateTime.Today.ToShortDateString();
            

            int newDatabaseID = -1;
            bool rollback = false;

            try
            {
                SqlConnection con;
                if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
                //else { con = new SqlConnection(SiteMaster.connectionStr); }
                else { con = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalDemoDatabase"].ConnectionString); }

                SqlTransaction transaction;
                int rowsAffected = 0;

                using (con)
                {
                    if (con.State == ConnectionState.Closed) con.Open();
                    transaction = con.BeginTransaction("insertEmpCert");
                    try
                    {

                        string insText =
                            "INSERT INTO [dbo].[EmployeeCertificate] (" +
                                "[EmployeeID],      [CertificateID]      ,[CertificateNumber]      ,[ApplicatorID]      ,[EmpReceiveDate]      ,[Comment]" +
                            ") VALUES (" +
                                "@EmployeeID, @CertificateID, @CertificateNumber, @ApplicatorID, @EmpReceiveDate, @Comment" +
                            ");";

                        SqlCommand insertCmd = new SqlCommand(insText, con);

                        SqlParameter[] insertParams = {
                            new SqlParameter() { ParameterName = "@EmployeeID",                    SqlDbType = SqlDbType.Int,  Value = empID      },
                            new SqlParameter() { ParameterName = "@CertificateID",              SqlDbType = SqlDbType.Int,  Value = certID       },
                            new SqlParameter() { ParameterName = "@CertificateNumber",            SqlDbType = SqlDbType.VarChar,  Value = certNum    },
                            new SqlParameter() { ParameterName = "@ApplicatorID",             SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(appID))? appID : (object)DBNull.Value     },
                            new SqlParameter() { ParameterName = "@EmpReceiveDate",            SqlDbType = SqlDbType.Date,     Value = (!string.IsNullOrEmpty(certDate))? certDate : (object)DBNull.Value       },
                            new SqlParameter() { ParameterName = "@Comment",         SqlDbType = SqlDbType.VarChar,     Value = (!string.IsNullOrEmpty(comm))? comm : (object)DBNull.Value    },
                            
                        };
                        insertCmd.Parameters.AddRange(insertParams);

                        insertCmd.Transaction = transaction;
                        rowsAffected = insertCmd.ExecuteNonQuery();
                        if (rowsAffected == 1)
                        {
                            string selectText = "SELECT TOP 1 [EmployeeCertificateID] FROM [dbo].[EmployeeCertificate] WHERE [EmployeeID] = @EmployeeID and [CertificateID] = @CertificateID and [EmpReceiveDate] = @EmpReceiveDate";
                            SqlCommand selectCmd = new SqlCommand(selectText, con);
                            selectCmd.Transaction = transaction;

                            SqlParameter[] selectParams = {
                                    new SqlParameter() { ParameterName = "@EmployeeID",       SqlDbType = SqlDbType.VarChar,  Value = empID      },
                                    new SqlParameter() { ParameterName = "@CertificateID",    SqlDbType = SqlDbType.VarChar,  Value = certID    },
                                    new SqlParameter() { ParameterName = "@EmpReceiveDate",   SqlDbType = SqlDbType.Date,  Value = certDate    }
                                };
                            selectCmd.Parameters.AddRange(selectParams);

                            using (SqlDataReader readMe = selectCmd.ExecuteReader())
                            {
                                while (readMe.Read())
                                {
                                    string i = readMe["EmployeeCertificateID"].ToString();
                                    newDatabaseID = Convert.ToInt32(i);
                                }
                            }
                            if (newDatabaseID > 0)
                            {
                                string insertHistText =
                                        "INSERT INTO [dbo].[CreateHistory] (" +
                                            "[TableName],[RowID],[DateCreated],[CreatedByUser]" +
                                        ") VALUES (" +
                                            "@Table, @RowID, @Date, @User" +
                                        ")";
                                SqlCommand insertHistCmd = new SqlCommand()
                                {
                                    Connection = con,
                                    CommandText = insertHistText,
                                    Transaction = transaction
                                };

                                SqlParameter[] insertHistParams = {
                                        new SqlParameter() { ParameterName = "@Table",      SqlDbType = SqlDbType.VarChar,  Value = "EmployeeCertificate"},
                                        new SqlParameter() { ParameterName = "@RowID",      SqlDbType = SqlDbType.Int,      Value = newDatabaseID  },
                                        new SqlParameter() { ParameterName = "@Date",       SqlDbType = SqlDbType.Date,     Value = today              },
                                        new SqlParameter() { ParameterName = "@User",       SqlDbType = SqlDbType.VarChar,  Value = logUser            }
                                    };
                                insertHistCmd.Parameters.AddRange(insertHistParams);
                                rowsAffected = insertHistCmd.ExecuteNonQuery();

                                if (rowsAffected == 1) transaction.Commit();
                                else
                                {
                                    transaction.Rollback(); //Hist table unable to be inserted
                                    rollback = true;

                                    string amount = rowsAffected > 1 ? " TOO MANY (" + rowsAffected.ToString() + ") " : " NO ";
                                    ErrorLabel.Text = "There was a problem saving your changes to the database: the [EmployeeCertificate] table was unable to be properly updated.";
                                    HintLabel.Text = "The update process of the [CreateHistory] table should insert ONE row into your table. This error indicates that there were" + amount + "rows inserted into the Create History database table instead, causing a rollback of all changes.";
                                }
                            }
                        }
                        else
                        {
                            transaction.Rollback(); //empcert table unable to be inserted
                            rollback = true;

                            string amount = rowsAffected > 1 ? " TOO MANY (" + rowsAffected.ToString() + ") " : " NO ";
                            ErrorLabel.Text = "There was a problem saving your changes to the database: the [EmployeeCertificate] table was unable to be properly updated.";
                            HintLabel.Text = "The update process of the [EmployeeCertificate] table should insert ONE row into your table. This error indicates that there were" + amount + "rows inserted into the EmployeeCertificate database table instead, causing a rollback of all changes.";
                        }

                    }
                    catch (Exception exTRANS)
                    {
                        try { transaction.Rollback(); }
                        finally
                        {
                            Console.WriteLine("Rollback Exception Type: {0}", exTRANS.GetType());
                            Console.WriteLine("  Message: {0}", exTRANS.Message);
                            rollback = true;
                        }
                    }
                }
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception exOUTER)
            {
                Console.WriteLine("Rollback Exception Type: {0}", exOUTER.GetType());
                Console.WriteLine("  Message: {0}", exOUTER.Message);
                rollback = true;
            }

            if (rollback)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalError');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalSuccess');", true);
            }

        }


    }
}