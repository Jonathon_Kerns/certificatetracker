﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Directory.aspx.cs" Inherits="FertCertWebApp.Directory" MasterPageFile="~/Site.Master"%>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="Content/Directory.css" type="text/css" media="screen" />

    
    <div class="jumbotron">
        <h1>View Directory: Companies and Employees</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

    <nav>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="emp-tab" data-toggle="tab" href="#emp" role="tab" aria-controls="emp" aria-selected="true">View Employees</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="comp-tab" data-toggle="tab" href="#comp" role="tab" aria-controls="comp" aria-selected="false">View Companies</a>
            </li>
        </ul>
    </nav>

    
    <div class="tab-content" id="myTabContent">


        <div class="container tab-pane fade show active cup-border" id="emp" role="tabpanel" aria-labelledby="emp-tab">
            <br />
            <br />
            <h3>View employees currently being tracked by this system.</h3>
            <hr />
            <asp:UpdatePanel ID="Panel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                        <asp:GridView ID="EmpGrid" runat="server" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" OnPageIndexChanging="EmpGrid_PageIndexChanging"  Width="100%"> 
                            <%--OnSorting="EmpGrid_Sorting"--%>
                            <HeaderStyle CssClass="modgrid-header" />
                            <RowStyle CssClass="modgrid-row-style" />
                            <AlternatingRowStyle CssClass="modgrid-alt-row-style" />
                            <FooterStyle CssClass="modgrid-footer" />
                            <PagerStyle CssClass="modgrid-footer" />
        
                            <Columns>
                                <asp:BoundField  HeaderText="Employee ID"        DataField="EmployeeID"                     ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                <asp:BoundField  HeaderText="First Name"         DataField="FirstName"                      ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center"/>
                                <asp:BoundField  HeaderText="Last Name"          DataField="LastName"                       ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center"/>
                                <asp:BoundField  HeaderText="Street Address"    DataField="StreetAddress1"                  ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center"/>
                                <asp:BoundField  HeaderText="Company ID"         DataField="CompanyID"                      ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                <asp:BoundField  HeaderText="Company Name"      DataField="CompanyName"                     ItemStyle-Width="20%"/>
                                <asp:BoundField  HeaderText="Branch Name"       DataField="CompanyBranchName"               ItemStyle-Width="20%"/>
                            </Columns>
                            <PagerSettings Mode="Numeric" Position="Bottom" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
        </div><%-- id="comp" role="tabpanel"--%>

        <div class="container tab-pane fade cup-border" id="comp" role="tabpanel" aria-labelledby="comp-tab">
            <br />
            <br />
            <h3>View companies currently being tracked by this system.</h3>
            <hr />

            <asp:UpdatePanel ID="Panel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                        <asp:GridView ID="CompGrid" runat="server" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" OnPageIndexChanging="CompGrid_PageIndexChanging"  Width="100%"> 
                            <%--OnSorting="CompGrid_Sorting"--%>
                            <HeaderStyle CssClass="modgrid-header" />
                            <RowStyle CssClass="modgrid-row-style" />
                            <AlternatingRowStyle CssClass="modgrid-alt-row-style" />
                            <FooterStyle CssClass="modgrid-footer" />
                            <PagerStyle CssClass="modgrid-footer" />
        
                            <Columns>
                                <asp:BoundField  HeaderText="CompanyID"         DataField="CompanyID"                     ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"/>
                                <asp:BoundField  HeaderText="Company Name"      DataField="CompanyName"                   ItemStyle-Width="40%"/>
                                <asp:BoundField  HeaderText="Branch Name"       DataField="CompanyBranchName"             ItemStyle-Width="50%"/>
                            </Columns>
                            <PagerSettings Mode="Numeric" Position="Bottom" />
                        </asp:GridView>
                     </ContentTemplate>
                </asp:UpdatePanel>
        </div> <%--id="comp" role="tabpanel"--%>
    </div> <%--id="myTabContent"--%>

 
</asp:Content>