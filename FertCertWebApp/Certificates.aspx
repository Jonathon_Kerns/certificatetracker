﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Certificates.aspx.cs" Inherits="FertCertWebApp.Certificates" MasterPageFile="~/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<link rel="stylesheet" href="Content/Certificate.css" type="text/css" media="screen" />

    <div class="jumbotron">
        <h1>View Certificates</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

    <div class="tab-content" id="myTabContent">
    <div class="container cup-border" >
            <br />
            <br />
            <h3>View certificates currently being employees in this system.</h3>
            <hr />

            

		    <asp:GridView ID="CertGrid" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" AllowSorting="true" OnPageIndexChanging="CertGrid_PageIndexChanging"  Width="100%"> 
                <%--OnSorting="CertGrid_Sorting"--%>
			    <HeaderStyle CssClass="modgrid-header" />
			    <RowStyle CssClass="modgrid-row-style" />
			    <AlternatingRowStyle CssClass="modgrid-alt-row-style" />
			    <FooterStyle CssClass="modgrid-footer" />
			    <PagerStyle CssClass="modgrid-footer" />

			    <Columns>
                    
				    <asp:BoundField  HeaderText="Certificate ID"       	DataField="CertificateID"           ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"/>
				    <asp:BoundField  HeaderText="Certificate Number"      DataField="CertificateNumber"		ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center"/>
				    <asp:BoundField  HeaderText="Received Date"        DataField="EmpReceiveDate"               ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}"/>
                    <asp:BoundField  HeaderText="Employee ID"       	DataField="EmployeeID"              ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"/>
				    <asp:BoundField  HeaderText="First Name"      		DataField="FirstName"                ItemStyle-Width="20%" />
				    <asp:BoundField  HeaderText="Last Name"          DataField="LastName"                ItemStyle-Width="20%"/>
				    
			    </Columns>
			    <PagerSettings Mode="Numeric" Position="Bottom" />
		    </asp:GridView>

    </div>
</div>
</asp:Content>
