﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FertCertWebApp
{
    public partial class MailMerge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        
        protected void getFileButton_Click(object sender, EventArgs e)
        {

            filelink.Visible = false;

            DataTable MailMergeTable = new DataTable("MailMergeTable");
            MailMergeTable.Columns.Add("EmployeeID", typeof(string));
            MailMergeTable.Columns.Add("FirstName", typeof(string));
            MailMergeTable.Columns.Add("LastName", typeof(string));
            MailMergeTable.Columns.Add("MiddleInitial", typeof(string));
            MailMergeTable.Columns.Add("CertificateNumber", typeof(string));
            MailMergeTable.Columns.Add("EmpReceiveDate", typeof(string));
            MailMergeTable.Columns.Add("CompanyName", typeof(string));
            MailMergeTable.Columns.Add("StreetAddress1", typeof(string));
            MailMergeTable.Columns.Add("StreetAddress2", typeof(string));
            MailMergeTable.Columns.Add("City", typeof(string));
            MailMergeTable.Columns.Add("AddressState", typeof(string));
            MailMergeTable.Columns.Add("Zip1", typeof(string));
            MailMergeTable.Columns.Add("Zip2", typeof(string));

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalDemoDatabase"].ConnectionString);
            string query = "SELECT   e.EmployeeID " +
                            "       , e.FirstName " +
                            "       , e.LastName " +
                            "       , e.MiddleInitial " +
                            "       , ec.CertificateNumber " +
                            "       , ec.EmpReceiveDate " +
                            "       , e.StreetAddress1 " +
                            "       , e.StreetAddress2 " +
                            "       , e.City " +
                            "       , e.AddressState " +
                            "       , e.Zip1 " +
                            "       , e.Zip2 " +
                            "FROM Employee e " +
                            "join EmployeeCertificate ec on e.EmployeeID = ec.EmployeeID where ec.CertificateID = 1 and ec.EmpReceiveDate between '" + startDatePicker.SelectedDate.ToShortDateString() + "' and '" + endDatePicker.SelectedDate.ToShortDateString() + "'";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = MailMergeTable.NewRow();

                    tableRow["EmployeeID"] = readMe["EmployeeID"].ToString();
                    tableRow["FirstName"] =readMe["FirstName"].ToString();
                    tableRow["LastName"] = readMe["LastName"].ToString();
                    tableRow["MiddleInitial"] = readMe["MiddleInitial"].ToString();
                    tableRow["CertificateNumber"] = readMe["CertificateNumber"].ToString();
                    tableRow["EmpReceiveDate"] = readMe["EmpReceiveDate"].ToString();
                    tableRow["StreetAddress1"] = readMe["StreetAddress1"].ToString();
                    tableRow["StreetAddress2"] = readMe["StreetAddress2"].ToString();
                    tableRow["City"] = readMe["City"].ToString();
                    tableRow["AddressState"] = readMe["AddressState"].ToString();
                    tableRow["Zip1"] = readMe["Zip1"].ToString();
                    tableRow["Zip2"] = readMe["Zip2"].ToString();

                    MailMergeTable.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }
            Session["MailMergeTable"] = MailMergeTable;

            //try
            //{
            //    var lines = new List<string>();

            //    string[] columnNames = MailMergeTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();

            //    var header = string.Join(",", columnNames.Select(name => $"\"{name}\""));
            //    lines.Add(header);

            //    var valueLines = MailMergeTable.AsEnumerable()
            //        .Select(row => string.Join(",", row.ItemArray.Select(val => $"\"{val}\"")));

            //    lines.AddRange(valueLines);

            //    File.WriteAllLines("MailMergeFile.csv", lines);
            //}
            //catch (Exception ex)
            //{
            //    Console.Write(ex.ToString());
            //}

            XLWorkbook workbook = new XLWorkbook("yourfilepathhere"); //SET FILEPATH OF EXCEL HERE FOR WHEN PROJECT GETS PUBLISHED TO SERVER-- watch escape characters!
            workbook.Worksheets.Add(MailMergeTable);
            filelink.Visible = true;

        }
    }
}