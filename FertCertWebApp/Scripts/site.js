﻿$(document).ready(function () {
    $('select#MainContent_CompDropDown_forBranch.form_list').hide();
    $('input#MainContent_CompRadio_Branch').prop("checked", false);

    //$('input#MainContent_branchCheckbox').prop("checked", false);
    //$('input#MainContent_compCheckbox').prop("checked", true);
    //$('input#MainContent_branchCheckbox.form-check-input').checked = false;
    //$('input#MainContent_compCheckbox.form-check-input').checked = true;
});

function openModal(name) {
    text = '#' + name;
    $(text).modal('show');
}

function setPillTab(name0, name1, name2) {
    active = '#' + name0;
    inactive1 = '#' + name1;
    inactive2 = '#' + name2;
    $(active).toggleClass('active');
    $(inactive1).removeClass('active');
    $(inactive2).removeClass('active');
}

function clickRadio(bool) {
    if (bool) {
        //$('input#CompNameBox').show();
        //$('input#CompDropDown_forBranch').hide();
        //$('input#CompBranchBox').text('Main Branch');
        $('input#MainContent_CompNameBox').show();
        $('select#MainContent_CompDropDown_forBranch.form_list').hide();
        $('input#MainContent_CompBranchBox').val('Main Branch');
        $('input#MainContent_CompRadio_Branch').prop("checked", false);

        $('input#MainContent_branchCheckbox.form-check-input').prop('checked', false);
        $('input#MainContent_compCheckbox.form-check-input').prop('checked', true);

        $('input#MainContent_branchCheckbox.form-check-input').removeClass('active');
        $('input#MainContent_compCheckbox.form-check-input').addClass('active');

    } else {
        $('input#MainContent_CompNameBox').hide();
        $('select#MainContent_CompDropDown_forBranch.form_list').show();
        $('input#MainContent_CompBranchBox').val('');
        $('input#MainContent_CompRadio_Branch').prop("checked", true);

        $('input#MainContent_branchCheckbox.form-check-input').prop('checked', true);
        $('input#MainContent_compCheckbox.form-check-input').prop('checked', false);

        $('input#MainContent_branchCheckbox.form-check-input').addClass('active');
        $('input#MainContent_compCheckbox.form-check-input').removeClass('active');
    }
}