﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateTracker.aspx.cs" Inherits="FertCertWebApp.CertificateTracker" MasterPageFile="~/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <link rel="stylesheet" href="Content/Certificate.css" type="text/css" media="screen" />

    <div class="jumbotron">
        <h1>Adding Employee Certificates</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

     <br />

    <div class="tab-content" id="myTabContent">
    <div class="container cup-border" >
        
    <h3>Add New Employee Certificate</h3>
    <p class="instructions">Use the form below to add a certificate to the database. 
        For a list of the certificates currently tracked by our database, please see the <a href="\Certificates">View Certificates</a> page.</p>
    <p class="instructions">Items marked with an asterisk (<span class="fa fa-asterisk fa-lg"></span>) are required fields.</p>
    <hr />
    <br />

    <div class="row">
	    <button type="button" class="open-collapsible" ><span class="fa fa-folder-open fa-lg pr-2"></span>   Enter Details<span class="fa fa-asterisk fa-lg pull-right"></span><span class="fa fa-asterisk fa-lg pull-right"></span></button>
    </div>
    <br />
    <div class="Row">
        <div class="col-md-4">
            <p class="form_label">Employee:</p>
        </div>
        <div class="col-md-8">
            <p><span class="fa fa-asterisk fa-lg pull-right"></span></p>
             <asp:DropDownList ID="Emp_DropDownList" 
                            OnSelectedIndexChanged="Emp_DropDownList_SelectedIndexChanged"
                            runat="server" 
                            AutoPostBack="True"
                            AppendDataBoundItems="true"
                            OnMouseDown="this.size=10;" 
                            OnFocusOut="this.size=1;" 
                            OnDblClick="this.size=1;">
            </asp:DropDownList>
        </div>
    </div>
    <br />
    <div class="Row">
        <div class="col-md-4">
            <p class="form_label">Certificate:</p>
        </div>
        <div class="col-md-8">
            <p><span class="fa fa-asterisk fa-lg pull-right"></span></p>
            <asp:DropDownList ID="Cert_DropDownList" 
                            OnSelectedIndexChanged="Cert_DropDownList_SelectedIndexChanged"
                            runat="server" 
                            AutoPostBack="True"
                            AppendDataBoundItems="true"
                            OnMouseDown="this.size=5;" 
                            OnFocusOut="this.size=1;" 
                            OnDblClick="this.size=1;">
            </asp:DropDownList>
        </div>
    </div>
    <br />

    <div class="Row">
         <div class="col-md-4">
            <p class="form_label">Date:</p>
        </div>
        <div class="col-md-8">
            <p><span class="fa fa-asterisk fa-lg pull-right"></span></p>
            <asp:Calendar ID="datePicker" runat="server" SelectionMode="Day" />
        </div>
    </div>
    <br />
    <div class="Row">
       <div class="col-md-4">
            <p class="form_label">Certificate Number:</p>
        </div>
        <div class="col-md-8">
            <asp:TextBox 
                runat="server" 
                ID="CertNumBox"
                AutoPostBack="False"
                CssClass="form_box"
                MaxLength="50"
                TextMode="Number"
                Width="100%"
                ToolTip="Enter Certificate Number.">
            </asp:TextBox>
            <p><span class="fa fa-asterisk fa-lg pull-right"></span></p>
        </div>
        
    </div>
    <br />

    <div class="Row">
        <div class="col-md-4">
            <p class="form_label">Applicator ID:</p>
        </div>
        <div class="col-md-8">
            <asp:TextBox 
                runat="server" 
                ID="ApplicatorBox"
                AutoPostBack="False"
                CssClass="form_box"
                MaxLength="255"
                Width="100%"
                ToolTip="Enter Applicator ID.">
            </asp:TextBox>
        </div>
    </div>

    <br />
    <div class="Row">
        <div class="col-md-4">
            <p class="form_label">Comment:</p>
        </div>
        <div class="col-md-8">
            <asp:TextBox 
                runat="server" 
                ID="CommentBox"
                AutoPostBack="False"
                CssClass="form_box"
                MaxLength="500"
                Width="100%"
                ToolTip="Enter comments.">
            </asp:TextBox>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-12">
            <asp:Button 
                runat="server" 
                ID="SaveNewCertButton" 
                Text="Add Employee Certificate Details"
                CssClass="save_button"
                OnClick="SaveNewCertButton_Click"
                ></asp:Button> 
        </div>
    </div>
</div></div>
    <div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="modalWarningLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header alert-primary" style="background-color: goldenrod; color: black;">
                    <h4 class="modal-title text-center" id="modalCompWarningLabel"><span class="fa fa-exclamation-triangle fa-lg pr-2"></span> WARNING, CHANGES NOT SAVED! <span class="fa fa-exclamation-triangle fa-lg pl-2"></span></h4>
                </div>
                <div class="modal-body">
                    <h4>ISSUES DETECTED WITH MANDATORY FIELDS.</h4>
                    <p>Your changes were not saved to the database. To continue, correct the inputs listed below.</p>
                    <hr />
                    <asp:Label runat="server" CssClass="instructions" ID="SaveWarning" Text=""></asp:Label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: goldenrod; color: black;">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="modalErrorLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header alert-primary" style="background-color: firebrick; color: white;">
                    <h4 class="modal-title text-center" id="modalErrorLabel"><span class="fa fa-times fa-lg pr-2"></span>ERROR, CHANGES NOT SAVED! <span class="fa fa-times fa-lg pl-2"></span></h4>
                </div>
                <div class="modal-body">
                    <h4>AN ERROR HAS OCCURRED. NO UPDATES WERE MADE.</h4>
                    <p>Your changes were NOT saved to the database. Please review the details below.</p>
                    <hr />

                    <h4 class="error_label">Error Summary: </h4>
                    <asp:Label runat="server" ID="ErrorLabel" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."></asp:Label>
                    <h4 class="error_label">Hint: </h4>
                    <asp:Label runat="server" ID="HintLabel" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."></asp:Label>

                    <hr />
                    <h4>Try to save your changes again.</h4>
                    <p>If you are repeatedly receiving this error, please make a note of the details of this error and contact a developer for assistance.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: firebrick; color: white;">CLOSE</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSuccessLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header alert-primary" style="background-color: limegreen; color: black;">
                    <h4 class="modal-title text-center" id="modalSuccessLabel"><span class="fa fa-check-circle"></span>SUCCESS! <span class="fa fa-check-circle"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <h4>CHANGES SAVED.</h4>
                        <hr />
                        <p>Your changes have been saved to the database successfully. </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: limegreen; color: black;">OK</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>