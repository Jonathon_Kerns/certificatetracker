﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FertCertWebApp
{
    public partial class Certificates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con;
            if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
            else { con = new SqlConnection(SiteMaster.SetString()); }

            if (!Page.IsPostBack)
            {
                Session.Clear();
                DataTable Certs = new DataTable("Certs");

                CreateTables(Certs);

                if (Certs.Rows.Count == 0) { Certs = PullCerts(con, Certs); }
                CertGrid.DataSource = Certs;
                CertGrid.DataBind();

                Session["Certs"] = Certs;

            }
            else
            {
                DataTable Certs = (DataTable)Session["Certs"];
                CertGrid.DataSource = Certs;
                CertGrid.DataBind();
            }

        }
        private void CreateTables(DataTable Certs)
        {
            Certs.Columns.Add("CertificateID", typeof(int));
            Certs.Columns.Add("CertificateNumber", typeof(string));
            Certs.Columns.Add("EmpReceiveDate", typeof(string));
            Certs.Columns.Add("EmployeeID", typeof(int));
            Certs.Columns.Add("FirstName", typeof(string));
            Certs.Columns.Add("LastName", typeof(string));
        }

        private DataTable PullCerts(SqlConnection con, DataTable Certs)
        {
            string query = "SELECT  e.[EmployeeID],e.[FirstName],e.[LastName], ec.[CertificateID], ec.CertificateNumber, ec.EmpReceiveDate " +
                            "FROM [dbo].[Employee] e " +
                            "join EmployeeCertificate ec on ec.EmployeeID = e.EmployeeID " +
                            "order by ec.EmpReceiveDate desc, e.[FirstName] asc, e.[LastName] asc";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = Certs.NewRow();

                    tableRow["EmployeeID"] = Convert.ToInt16(readMe["EmployeeID"]);
                    tableRow["FirstName"] = readMe["FirstName"].ToString();
                    tableRow["LastName"] = readMe["LastName"].ToString();

                    tableRow["CertificateID"] = Convert.ToInt16(readMe["CertificateID"]);
                    tableRow["CertificateNumber"] = readMe["CertificateNumber"].ToString();
                    tableRow["EmpReceiveDate"] = readMe["EmpReceiveDate"].ToString();

                    Certs.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }

            Session["Emps"] = Certs;
            return Certs;
        }

        protected void CertGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CertGrid.PageIndex = e.NewPageIndex;
            CertGrid.DataBind();
        }
    }
}