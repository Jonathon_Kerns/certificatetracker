﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FertCertWebApp
{
    public partial class History : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con;
            if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
            else { con = new SqlConnection(SiteMaster.SetString()); }

            if (!Page.IsPostBack)
            {
                Session.Clear();
                DataTable CompanyHist = new DataTable("CompanyHist"); 
                DataTable CertificateHist = new DataTable("CertificateHist"); 
                DataTable EmployeeHist = new DataTable("EmployeeHist");
                DataTable EditHist = new DataTable("EditHist");
                CreateTables(CompanyHist, CertificateHist, EmployeeHist, EditHist);

                if (CompanyHist.Rows.Count == 0) { CompanyHist = PullCompanyCreate(con, CompanyHist); }
                CompHistGrid.DataSource = CompanyHist;
                CompHistGrid.DataBind();

                if (CertificateHist.Rows.Count == 0) { CertificateHist = PullCertificateCreate(con, CertificateHist); }
                CertHistGrid.DataSource = CertificateHist;
                CertHistGrid.DataBind();

                if (EmployeeHist.Rows.Count == 0) { EmployeeHist = PullEmployeeCreate(con, EmployeeHist); }
                EmpHistGrid.DataSource = EmployeeHist;
                EmpHistGrid.DataBind();

                if (EditHist.Rows.Count == 0) { EditHist = PullEdits(con, EditHist); }
                EditHistGrid.DataSource = EditHist;
                EditHistGrid.DataBind();

                Session["CompanyHist"] = CompanyHist;
                Session["CertificateHist"] = CertificateHist;
                Session["EmployeeHist"] = EmployeeHist;
                Session["EditHist"] = EditHist;
                Session["activePill"] = "[pills-comp-tab, pills-emp-tab, pills-cert-tab]";

            }
            else
            {
                DataTable CompanyHist = (DataTable)Session["CompanyHist"];
                DataTable CertificateHist = (DataTable)Session["CertificateHist"];
                DataTable EmployeeHist = (DataTable)Session["EmployeeHist"];
                DataTable EditHist = (DataTable)Session["EditHist"];
                //string active = (string)Session["activePill"].ToString().Replace('[', ' ').Replace(']', ' ');
                //active = string.Concat(active.Where(c => !char.IsWhiteSpace(c)));
                //string[] activeAr = active.Split(',');
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pill", "setPillTab('"+ activeAr[0]+","+activeAr[1] + "," + activeAr[2] + "');", true);
            }

        }
        
        private void CreateTables(DataTable CompanyHist, DataTable CertificateHist, DataTable EmployeeHist, DataTable EditHist)
        {
            //EditHist = new DataTable("EditHist");            
            EditHist.Columns.Add("HistoryID", typeof(int));
            EditHist.Columns.Add("TableName", typeof(string));
            EditHist.Columns.Add("RowID", typeof(int));
            EditHist.Columns.Add("ColumnName", typeof(string));
            EditHist.Columns.Add("OldValue", typeof(string));
            EditHist.Columns.Add("NewValue", typeof(string));
            EditHist.Columns.Add("DateModified", typeof(string));
            EditHist.Columns.Add("ModifiedByUser", typeof(string));

            //CompanyHist = new DataTable("CompanyHist");            
            CompanyHist.Columns.Add("HistoryID", typeof(int));
            CompanyHist.Columns.Add("CompanyID", typeof(int));
            CompanyHist.Columns.Add("CompanyName", typeof(string));
            CompanyHist.Columns.Add("CompanyBranchName", typeof(string));
            CompanyHist.Columns.Add("DateCreated", typeof(string));
            CompanyHist.Columns.Add("CreatedByUser", typeof(string));
        

            //EmployeeHist = new DataTable("EmployeeHist");
            EmployeeHist.Columns.Add("HistoryID", typeof(int));
            EmployeeHist.Columns.Add("EmployeeID", typeof(int));
            EmployeeHist.Columns.Add("FirstName", typeof(string));
            EmployeeHist.Columns.Add("LastName", typeof(string));
            EmployeeHist.Columns.Add("CompanyID", typeof(int));
            EmployeeHist.Columns.Add("CompanyName", typeof(string));
            EmployeeHist.Columns.Add("CompanyBranchName", typeof(string));
            EmployeeHist.Columns.Add("DateCreated", typeof(string));
            EmployeeHist.Columns.Add("CreatedByUser", typeof(string));

            //CertificateHist = new DataTable("CertificateHist");
            CertificateHist.Columns.Add("HistoryID", typeof(int));
            CertificateHist.Columns.Add("EmployeeCertificateID", typeof(int));
            CertificateHist.Columns.Add("EmployeeID", typeof(int));
            CertificateHist.Columns.Add("FirstName", typeof(string));
            CertificateHist.Columns.Add("LastName", typeof(string));
            CertificateHist.Columns.Add("CompanyID", typeof(int));
            CertificateHist.Columns.Add("CompanyName", typeof(string));
            CertificateHist.Columns.Add("CompanyBranchName", typeof(string));
            CertificateHist.Columns.Add("CertificateID", typeof(int));
            CertificateHist.Columns.Add("CertificateName", typeof(string));
            CertificateHist.Columns.Add("CertificateNumber", typeof(int));
            CertificateHist.Columns.Add("EmpReceiveDate", typeof(string));
            CertificateHist.Columns.Add("DateCreated", typeof(string));
            CertificateHist.Columns.Add("CreatedByUser", typeof(string));
        }
        
        private DataTable PullCompanyCreate(SqlConnection con, DataTable CompanyHist)
        {
            

            string query =  "SELECT   ch.HistoryID "         +
                            "       , ch.TableName "         +
                            "       , ch.DateCreated "       +
                            "       , ch.CreatedByUser "     +
                            "       , c.CompanyID "          +
                            "       , c.CompanyName "        +
                            "       , c.CompanyBranchName "  +
                            "FROM CreateHistory ch "         +
                            "join Company c on c.CompanyID = ch.RowID where ch.TableName = 'Company' order by ch.DateCreated DESC ";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = CompanyHist.NewRow();

                    tableRow["HistoryID"] = Convert.ToInt16(readMe["HistoryID"]);
                    tableRow["CompanyID"] = Convert.ToInt16(readMe["CompanyID"]);
                    tableRow["CompanyName"] = readMe["CompanyName"].ToString();
                    tableRow["CompanyBranchName"] = readMe["CompanyBranchName"].ToString();
                    tableRow["DateCreated"] = Convert.ToDateTime(readMe["DateCreated"]).Date;
                    tableRow["CreatedByUser"] = readMe["CreatedByUser"].ToString();

                    CompanyHist.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }

            return CompanyHist;
        }

        private DataTable PullEmployeeCreate(SqlConnection con, DataTable EmployeeHist)
        {
            

            string query =  "SELECT   ch.HistoryID "            +
                            "       , ch.TableName "            +
                            "       , ch.DateCreated "          +
                            "       , ch.CreatedByUser "        +
                            "       , e.EmployeeID "            +
                            "       , e.FirstName "             +
                            "       , e.LastName "              +
                            "       , c.CompanyID "             +
                            "       , c.CompanyName "           +
                            "       , c.CompanyBranchName "     +
                            "FROM CreateHistory ch "            +
                            "join CompanyEmployee ce on ce.CompanyEmployeeID = ch.RowID " +
                            "join Employee e on e.EmployeeID = ce.EmployeeID "          +
                            "join Company c on c.CompanyID = ce.CompanyID "             +
                            "where ch.TableName = 'CompanyEmployee' order by ch.DateCreated DESC ";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = EmployeeHist.NewRow();

                    tableRow["HistoryID"] = Convert.ToInt16(readMe["HistoryID"]);
                    tableRow["EmployeeID"] = Convert.ToInt16(readMe["EmployeeID"]);
                    tableRow["FirstName"] = readMe["FirstName"].ToString();
                    tableRow["LastName"] = readMe["LastName"].ToString();
                    tableRow["CompanyID"] = Convert.ToInt16(readMe["CompanyID"]);
                    tableRow["CompanyName"] = readMe["CompanyName"].ToString();
                    tableRow["CompanyBranchName"] = readMe["CompanyBranchName"].ToString();
                    tableRow["DateCreated"] = Convert.ToDateTime(readMe["DateCreated"]).Date;
                    tableRow["CreatedByUser"] = readMe["CreatedByUser"].ToString();
                    
                    EmployeeHist.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }

            return EmployeeHist;
        }

        private DataTable PullCertificateCreate(SqlConnection con, DataTable CertificateHist)
        {
            

            string query =  "SELECT   ch.HistoryID "            +
                            "       , ch.TableName "            +
                            "       , ch.DateCreated "          +
                            "       , ch.CreatedByUser "        +
                            "       , e.EmployeeID "            +
                            "       , e.FirstName "             +
                            "       , e.LastName "              +
                            "       , c.CompanyID "             +
                            "       , c.CompanyName "           +
                            "       , c.CompanyBranchName "     +
                            "	   , tc.CertificateID "         +
                            "	   , tc.CertificateName "       +
                            "	   , ec.EmpReceiveDate "        +
                            "      , ec.EmployeeCertificateID " +
                            "      , ec.CertificateNumber "     +
                            "FROM CreateHistory ch "            +
                            "join EmployeeCertificate ec on ec.EmployeeCertificateID = ch.RowID "   +
                            "join CompanyEmployee ce on ce.EmployeeID = ec.EmployeeID "             +
                            "join Employee e on e.EmployeeID = ce.EmployeeID "                      +
                            "join Company c on c.CompanyID = ce.CompanyID "                         +
                            "join TheCertificate tc on tc.CertificateID = ec.CertificateID "        +
                            "where ch.TableName = 'EmployeeCertificate' order by ch.DateCreated DESC ";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = CertificateHist.NewRow();

                    tableRow["HistoryID"] = Convert.ToInt16(readMe["HistoryID"]);
                    tableRow["EmployeeCertificateID"] = Convert.ToInt16(readMe["EmployeeCertificateID"]);
                    tableRow["EmployeeID"] = Convert.ToInt16(readMe["EmployeeID"]);
                    tableRow["FirstName"] = readMe["FirstName"].ToString();
                    tableRow["LastName"] = readMe["LastName"].ToString();
                    tableRow["CompanyID"] = Convert.ToInt16(readMe["CompanyID"]);
                    tableRow["CompanyName"] = readMe["CompanyName"].ToString();
                    tableRow["CompanyBranchName"] = readMe["CompanyBranchName"].ToString();
                    tableRow["CertificateID"] = Convert.ToInt16(readMe["CertificateID"]);
                    tableRow["CertificateName"] = readMe["CertificateName"].ToString();
                    tableRow["CertificateNumber"] = Convert.ToInt16(readMe["CertificateNumber"]);
                    tableRow["EmpReceiveDate"] = Convert.ToDateTime(readMe["EmpReceiveDate"]).Date;
                    tableRow["DateCreated"] = Convert.ToDateTime(readMe["DateCreated"]).Date;
                    tableRow["CreatedByUser"] = readMe["CreatedByUser"].ToString();
                    
                    CertificateHist.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }

            return CertificateHist;
        }

        private DataTable PullEdits(SqlConnection con, DataTable EditHist)
        {


            string query = "SELECT   c.HistoryID " +
                            "       , c.TableName " +
                            "       , c.RowID " +
                            "       , c.ColumnName " +
                            "       , c.OldValue " +
                            "       , c.NewValue " +
                            "       , c.DateModified " +
                            "       , c.ModifiedByUser " +
                            "FROM EditHistory c where 1=1 order by c.DateModified DESC ";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = EditHist.NewRow();

                    tableRow["HistoryID"] = Convert.ToInt16(readMe["HistoryID"]);
                    tableRow["TableName"] = readMe["TableName"].ToString();
                    tableRow["RowID"] = Convert.ToInt16(readMe["RowID"]);
                    tableRow["ColumnName"] = readMe["ColumnName"].ToString();
                    tableRow["OldValue"] = readMe["OldValue"].ToString();
                    tableRow["NewValue"] = readMe["NewValue"].ToString();
                    tableRow["DateModified"] = Convert.ToDateTime(readMe["DateModified"]).Date;
                    tableRow["ModifiedByUser"] = readMe["ModifiedByUser"].ToString();

                    EditHist.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }

            return EditHist;
        }
        protected void CompHistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CompHistGrid.PageIndex = e.NewPageIndex;
            CompHistGrid.DataBind();
        }
        protected void EmpHistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            EmpHistGrid.PageIndex = e.NewPageIndex;
            EmpHistGrid.DataBind();
        }
        protected void CertHistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CertHistGrid.PageIndex = e.NewPageIndex;
            CertHistGrid.DataBind();
        }

        protected void EditHistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            EditHistGrid.PageIndex = e.NewPageIndex;
            EditHistGrid.DataBind();
        }
        //protected void CompHistGrid_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    DataTable CompanyHist = (DataTable)Session["CompanyHist"];
        //    CompanyHist.DefaultView.Sort = e.SortExpression; //+ " " + GetSortDirection(e.SortExpression);
        //    Session["CompanyHist"] = CompanyHist;
        //    Session["activePill"] = "[pills-comp-tab, pills-emp-tab, pills-cert-tab]";
        //    CompHistGrid.DataSource = CompanyHist;
        //    CompHistGrid.DataBind();
        //}
        //protected void EmpHistGrid_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    DataTable EmployeeHist = (DataTable)Session["EmployeeHist"];
        //    EmployeeHist.DefaultView.Sort = e.SortExpression; //+ " " + GetSortDirection(e.SortExpression);
        //    Session["EmployeeHist"] = EmployeeHist;
        //    Session["activePill"] = "[pills-emp-tab, pills-comp-tab, pills-cert-tab]";
        //    EmpHistGrid.DataSource = EmployeeHist;
        //    EmpHistGrid.DataBind();
        //}
        //protected void CertHistGrid_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    DataTable CertificateHist = (DataTable)Session["CertificateHist"];
        //    CertificateHist.DefaultView.Sort = e.SortExpression; //+ " " + GetSortDirection(e.SortExpression);
        //    Session["CertificateHist"] = CertificateHist;
        //    Session["activePill"] = "[pills-cert-tab, pills-comp-tab, pills-emp-tab]";
        //    CertHistGrid.DataSource = CertificateHist;
        //    CertHistGrid.DataBind();

        //}

    }
}