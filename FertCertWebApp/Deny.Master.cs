﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FertCertWebApp
{
    public partial class Deny : System.Web.UI.MasterPage
    {
        private List<string> _authorizedGroupsList = new List<string>()
       {
           //Below are possible user groups, tbh-- cannot tell which of these works so all included:
           
         //@"city.talgov.com\Retirement"
         @"city.talgov.com\ISS_Gis"//, 
         //@"city.talgov.com\ISS_Gis"
       };
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        override protected void OnInit(EventArgs e)
        {
            ///////////////////////////////
            if (IsUserAuthorized())
            {
                Server.Transfer("Default.aspx");
            }  else
            {
                Server.Transfer("Denied.aspx");
            }
            ///////////////////////////////
        }
        private bool IsUserAuthorized()
        {
            for (int i = 0; i < _authorizedGroupsList.Count; i++)
            {
                try
                {
                    //if (Page.User.IsInRole(_authorizedGroupsList[i]))
                    //{
                    //    return true;
                    //}
                    if (HttpContext.Current.User.IsInRole(_authorizedGroupsList[i]))
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    // not doing anything exceptions right now
                    continue;
                }
            }
            return false;
        }
    }
}