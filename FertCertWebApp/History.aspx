﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="FertCertWebApp.History" MasterPageFile="~/Site.Master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="jumbotron">
        <h1>View History</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

    <nav>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="comp" aria-selected="true">Created</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="mod-tab" data-toggle="tab" href="#mod" role="tab" aria-controls="mod" aria-selected="false">Modified</a>
            </li>
        </ul>
    </nav>

    <div class="tab-content" id="myTabContent">
        <div class="container tab-pane fade show active cup-border" id="create" role="tabpanel" aria-labelledby="create-tab">
            <br />
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                      <li class="nav-item">
                            <a class="nav-link active" id="pills-comp-tab" data-toggle="pill" href="#pills-comp" role="tab" aria-controls="pills-comp" aria-selected="true">Companies</a>
                      </li>
                      <li class="nav-item">
                            <a class="nav-link" id="pills-emp-tab" data-toggle="pill" href="#pills-emp" role="tab" aria-controls="pills-emp" aria-selected="false">Employees</a>
                      </li>
                      <li class="nav-item">
                            <a class="nav-link" id="pills-cert-tab" data-toggle="pill" href="#pills-cert" role="tab" aria-controls="pills-cert" aria-selected="false">Certificates</a>
                      </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                      <div class="tab-pane fade show active" id="pills-comp" role="tabpanel" aria-labelledby="pills-comp-tab">
                          <%--comp--%> 
                          <asp:UpdatePanel ID="Panel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                  <asp:GridView ID="CompHistGrid" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" AllowSorting="true" OnPageIndexChanging="CompHistGrid_PageIndexChanging"  Width="100%"> 
                                      <%--OnSorting="CompHistGrid_Sorting"--%>
                                        <HeaderStyle CssClass="modgrid-header" />
                                        <RowStyle CssClass="modgrid-row-style" />
                                        <AlternatingRowStyle CssClass="modgrid-alt-row-style" />
                                        <FooterStyle CssClass="modgrid-footer" />
                                        <PagerStyle CssClass="modgrid-footer" />
        
                                        <Columns>
                                            <asp:BoundField  HeaderText="HistoryID"         DataField="HistoryID"               SortExpression="HistoryID"              ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="CompanyID"         DataField="CompanyID"               SortExpression="CompanyID"              ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="Company Name"      DataField="CompanyName"             SortExpression="CompanyName"            ItemStyle-Width="45%"/>
                                            <asp:BoundField  HeaderText="Branch Name"       DataField="CompanyBranchName"       SortExpression="CompanyBranchName"      ItemStyle-Width="25%"/>
                                            <asp:BoundField  HeaderText="Date Created"      DataField="DateCreated"             SortExpression="DateCreated"            ItemStyle-Width="10%"   DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center"/>                           
                                            <asp:BoundField  HeaderText="Created By User"   DataField="CreatedByUser"           SortExpression="CreatedByUser"          ItemStyle-Width="10%"/>
                                        </Columns>
                                        <PagerSettings Mode="Numeric" Position="Bottom" />
                                    </asp:GridView>
                                </ContentTemplate>
                          </asp:UpdatePanel>
                      </div>

                      <div class="tab-pane fade" id="pills-emp" role="tabpanel" aria-labelledby="pills-emp-tab">
                          <%--emp--%> 
                          <asp:UpdatePanel ID="Panel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                  <asp:GridView ID="EmpHistGrid" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" AllowSorting="true" OnPageIndexChanging="EmpHistGrid_PageIndexChanging" Width="100%">
                                        <%--OnSorting="EmpHistGrid_Sorting" OnSorted=""--%> 
                                        <HeaderStyle CssClass="modgrid-header" />
                                        <RowStyle CssClass="modgrid-row-style" />
                                        <AlternatingRowStyle CssClass="modgrid-alt-row-style" />
                                        <FooterStyle CssClass="modgrid-footer" />
                                        <PagerStyle CssClass="modgrid-footer" />
        
                                        <Columns>
                                            <asp:BoundField  HeaderText="HistoryID"         DataField="HistoryID"               SortExpression="HistoryID"              ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="EmployeeID"        DataField="EmployeeID"              SortExpression="EmployeeID"             ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="First Name"        DataField="FirstName"               SortExpression="FirstName"              ItemStyle-Width="17.5%"/>
                                            <asp:BoundField  HeaderText="Last Name"         DataField="LastName"                SortExpression="LastName"               ItemStyle-Width="17.5%"/>
                                            <asp:BoundField  HeaderText="CompanyID"         DataField="CompanyID"               SortExpression="CompanyID"              ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="Company Name"      DataField="CompanyName"             SortExpression="CompanyName"            ItemStyle-Width="20%"/>
                                            <asp:BoundField  HeaderText="Branch Name"       DataField="CompanyBranchName"       SortExpression="CompanyBranchName"      ItemStyle-Width="10%"/>
                                            <asp:BoundField  HeaderText="Date Created"      DataField="DateCreated"             SortExpression="DateCreated"            ItemStyle-Width="10%"   DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center"/>                           
                                            <asp:BoundField  HeaderText="Created By User"   DataField="CreatedByUser"           SortExpression="CreatedByUser"          ItemStyle-Width="10%"/>
                                        </Columns>
                                        <PagerSettings Mode="Numeric" Position="Bottom" />
                                    </asp:GridView>
                                </ContentTemplate>

                          </asp:UpdatePanel>
                      </div>

                      <div class="tab-pane fade" id="pills-cert" role="tabpanel" aria-labelledby="pills-cert-tab">
                          <%--cert--%> 
                          <asp:UpdatePanel ID="Panel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                  <asp:GridView ID="CertHistGrid" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" AllowSorting="true" OnPageIndexChanging="CertHistGrid_PageIndexChanging"  Width="100%">
                                        <%--OnSorting="CertHistGrid_Sorting" OnSorted=""--%> 
                                        <HeaderStyle CssClass="modgrid-header" />
                                        <RowStyle CssClass="modgrid-row-style" />
                                        <AlternatingRowStyle CssClass="modgrid-alt-row-style" />
                                        <FooterStyle CssClass="modgrid-footer" />
                                        <PagerStyle CssClass="modgrid-footer" />
        
                                        <Columns>
                                            <asp:BoundField  HeaderText="HistoryID"                 DataField="HistoryID"               SortExpression="HistoryID"              ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="Employee Certificate ID"   DataField="EmployeeCertificateID"   SortExpression="EmployeeCertificateID"  ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="EmployeeID"                DataField="EmployeeID"              SortExpression="EmployeeID"             ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="First Name"                DataField="FirstName"               SortExpression="FirstName"              ItemStyle-Width="12%"/>
                                            <asp:BoundField  HeaderText="Last Name"                 DataField="LastName"                SortExpression="LastName"               ItemStyle-Width="12%"/>
                                            <asp:BoundField  HeaderText="CompanyID"                 DataField="CompanyID"               SortExpression="CompanyID"              ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="Company Name"              DataField="CompanyName"             SortExpression="CompanyName"            ItemStyle-Width="12%"/>
                                            <asp:BoundField  HeaderText="Branch Name"               DataField="CompanyBranchName"       SortExpression="CompanyBranchName"      ItemStyle-Width="12%"/>
                                            <asp:BoundField  HeaderText="Certificate ID"            DataField="CertificateID"           SortExpression="CertificateID"          ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="Certificate Name"          DataField="CertificateName"         SortExpression="CertificateName"        ItemStyle-Width="8%"/>
                                            <asp:BoundField  HeaderText="Certificate Number"        DataField="CertificateNumber"       SortExpression="CertificateNumber"      ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center"/>
                                            <asp:BoundField  HeaderText="Received Date"             DataField="EmpReceiveDate"          SortExpression="EmpReceiveDate"         ItemStyle-Width="8%"   DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center"/>                           
                                            <asp:BoundField  HeaderText="Date Created"              DataField="DateCreated"             SortExpression="DateCreated"            ItemStyle-Width="8%"   DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center"/>                           
                                            <asp:BoundField  HeaderText="Created By User"           DataField="CreatedByUser"           SortExpression="CreatedByUser"          ItemStyle-Width="8%"/>
                                        </Columns>
                                        <PagerSettings Mode="Numeric" Position="Bottom" />
                                    </asp:GridView>
                                </ContentTemplate>

                          </asp:UpdatePanel>
                      </div>



                </div>
            <br />
        </div>
        <div class="container tab-pane fade show cup-border" id="mod" role="tabpanel" aria-labelledby="mod-tab">
            
            <br />
            <asp:UpdatePanel ID="Panel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
		            <asp:GridView ID="EditHistGrid" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" AllowSorting="true" OnPageIndexChanging="EditHistGrid_PageIndexChanging"  Width="100%"> 

			            <HeaderStyle CssClass="modgrid-header" />
			            <RowStyle CssClass="modgrid-row-style" />
			            <AlternatingRowStyle CssClass="modgrid-alt-row-style" />
			            <FooterStyle CssClass="modgrid-footer" />
			            <PagerStyle CssClass="modgrid-footer" />

			            <Columns>
				            <asp:BoundField  HeaderText="HistoryID"         DataField="HistoryID"		ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
				            <asp:BoundField  HeaderText="Table Name"        DataField="TableName"       ItemStyle-Width="20%" />
				            <asp:BoundField  HeaderText="RowID"      		DataField="RowID"           ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"/>
				            <asp:BoundField  HeaderText="Column Name"       DataField="ColumnName"      ItemStyle-Width="30%"/>
				            <asp:BoundField  HeaderText="Old Value"       	DataField="OldValue"        ItemStyle-Width="10%"/>
				            <asp:BoundField  HeaderText="New Value"       	DataField="NewValue"        ItemStyle-Width="10%"/>
				            <asp:BoundField  HeaderText="Date Modified"      DataField="DateModified"      ItemStyle-Width="10%"   DataFormatString="{0:d}" ItemStyle-HorizontalAlign="Center"/>                           
				            <asp:BoundField  HeaderText="Modified By User"   DataField="ModifiedByUser"    ItemStyle-Width="10%"/>
			            </Columns>
			            <PagerSettings Mode="Numeric" Position="Bottom" />
		            </asp:GridView>
                </ContentTemplate>

            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>