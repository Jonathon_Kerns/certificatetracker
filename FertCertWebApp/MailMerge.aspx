﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailMerge.aspx.cs" Inherits="FertCertWebApp.MailMerge" MasterPageFile="~/Site.Master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Create Mail Merge</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

    <div class="tab-content" id="myTabContent">
    <div class="container cup-border" > 
    <br />
    <h3>Mail Merge Files By Date</h3>
    <p class="instructions">Use the form below to create your mail merge file according to a specified date range.</p>
    <hr />
    <br />
    <div class="row">
	    <button type="button" class="open-collapsible" ><span class="fa fa-calendar fa-lg pr-2"></span>   File Details<span class="fa fa-asterisk fa-lg pull-right"></span><span class="fa fa-asterisk fa-lg pull-right"></span></button>
    </div>
    <br />
    <div class="Row">
         <div class="col-md-4">
            <p class="form_label">Start Date:</p>
        </div>
        <div class="col-md-8">
            <p><span class="fa fa-asterisk fa-lg pull-right"></span></p>
            <asp:Calendar ID="startDatePicker" runat="server" SelectionMode="Day" />
        </div>
    </div>
    <br />

    <div class="Row">
         <div class="col-md-4">
            <p class="form_label">End Date:</p>
        </div>
        <div class="col-md-8">
            <p><span class="fa fa-asterisk fa-lg pull-right"></span></p>
            <asp:Calendar ID="endDatePicker" runat="server" SelectionMode="Day" />
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-12">
            <asp:Button 
                runat="server" 
                ID="getFileButton" 
                Text="Save New Company Details"
                CssClass="Pull Mail Merge File"
                OnClick="getFileButton_Click"
                ></asp:Button> 
        </div>
    </div>
        <br />
    <asp:Panel ID="filelink" runat="server" Visible="false">
        <a href="/file.xlsx">Click here to Download</a>
    </asp:Panel>

</div></div>
</asp:Content>
