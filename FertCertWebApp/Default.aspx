﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FertCertWebApp._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Certificate Tracker Portal</h1>
        <p class="lead">UU Stormwater Certification Tracker</p>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>(??) Progress Tracking (??)</h2>
            <a class="btn btn-default" href="\Progress"><p>(??) View Your Charts and Graphs! (??) &raquo;</p></a>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h2>Certificates</h2>
            <p>Stuff about Certs!</p>
            <ul>
                <li> <a class="btn btn-default" href="\Certificates"><p>View Recently Tracked Certificates &raquo;</p></a></li>
                <li> <a class="btn btn-default" href="\CertificateTracker"><p>Insert Certificate Completion Dates for Existing Employees &raquo;</p></a></li>
                <li> <a class="btn btn-default" href=""><p>(??) Add new Types of Certificates (??) &raquo;</p></a></li>
            </ul>
           <%-- <p>
                <a class="btn btn-default" href="">Learn more &raquo;</a>
            </p>--%>
        </div>
        <div class="col-md-6">
            <h2>Companies & Employees</h2>
            <p>Your Directory!</p>
            <ul>
                <li> <a class="btn btn-default" href="\Directory"><p>View Existing Companies and Employees &raquo;</p></a></li>
                <li> <a class="btn btn-default" href=""><p>(??) Modify and Remove Existing Companies and Employees (??) &raquo;</p></a></li>
                <li> <a class="btn btn-default" href="\AddNew"><p>Add new Companies and Employees &raquo;</p></a></li>
            </ul>
            <%--<p>
                <a class="btn btn-default" href="">Learn more &raquo;</a>
            </p>--%>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>History</h2>
            <p>Track Changes Over Time</p>
            <ul>
                <li><p>View Data Updates</p></li>
                <li><p>Track Who has Made Changes</p></li>
                <li><p>(??) View Mail Merge File Creation History (??)</p></li>
            </ul>
            <p>
                <a class="btn btn-default" href="\History">View &raquo;</a>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Mail Merge</h2>
            <p>Generate an Excel File to produce your Mail Merge Emails</p>
            <p>
                <a class="btn btn-default" href="\MailMerge">Create File &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
