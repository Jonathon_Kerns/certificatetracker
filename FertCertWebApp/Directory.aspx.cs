﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace FertCertWebApp
{
    public partial class Directory : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con;
            if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
            else { con = new SqlConnection(SiteMaster.SetString()); }

            if (!Page.IsPostBack)
            {
                Session.Clear();
                DataTable Emps = new DataTable("Emps");
                DataTable Comps = new DataTable("Comps");
                
                CreateTables(Emps, Comps);

                if (Emps.Rows.Count == 0) { Emps = PullEmps(con, Emps); }
                EmpGrid.DataSource = Emps;
                EmpGrid.DataBind();

                if (Comps.Rows.Count == 0) { Comps = PullComps(con, Comps); }
                CompGrid.DataSource = Comps;
                CompGrid.DataBind();

                Session["Emps"] = Emps;
                Session["Comps"] = Comps;

            }
            else
            {
                DataTable Emps = (DataTable)Session["Emps"];
                EmpGrid.DataSource = Emps;
                EmpGrid.DataBind();

                DataTable Comps = (DataTable)Session["Comps"];
                CompGrid.DataSource = Comps;
                CompGrid.DataBind();
            }

        }//Page_Load

        private void CreateTables(DataTable Emps, DataTable Comps)
        {
            //Emps = new DataTable("Emps");            
            Emps.Columns.Add("EmployeeID", typeof(int));
            Emps.Columns.Add("FirstName", typeof(string));
            Emps.Columns.Add("LastName", typeof(string));
            Emps.Columns.Add("StreetAddress1", typeof(string));
            Emps.Columns.Add("CompanyID", typeof(int));
            Emps.Columns.Add("CompanyName", typeof(string));
            Emps.Columns.Add("CompanyBranchName", typeof(string));

            //Comps = new DataTable("Comps");            
            Comps.Columns.Add("CompanyID", typeof(int));
            Comps.Columns.Add("CompanyName", typeof(string));
            Comps.Columns.Add("CompanyBranchName", typeof(string));

        }
        //private class CompanyItem
        //{
        //    protected int CompanyID { get; set; }
        //    protected string CompanyName { get; set; }
        //    protected string CompanyBranchName { get; set; }
        //    protected string StreetAddress1 { get; set; }
        //    protected string StreetAddress2 { get; set; }
        //    protected string City { get; set; }
        //    protected string State { get; set; }
        //    protected string Zip1 { get; set; }
        //    protected string Zip2 { get; set; }
        //    protected string PrimaryContact{ get; set; } //first and last
        //    protected string PrimaryContactPhone { get; set; } //with extension
        //    protected string PrimaryContactEmail { get; set; } //name and domain
        //    protected int NumberOfEmployees { get; set; } //sql grouping
        //    protected DateTime DateCreated { get; set; }
        //    protected string CreatedByUser { get; set; } //with extension
        //    protected DateTime DateModified { get; set; } //with extension
        //    protected string ModifiedByUser { get; set; } //with extension
        //} //class CompanyItem

        //private class EmployeeItem
        //{
        //    protected int EmployeeID { get; set; }
        //    protected string CompanyName { get; set; }
        //    protected string CompanyBranchName { get; set; }
        //    protected string CompanyID { get; set; }            
        //    protected DateTime StartDate { get; set; }
        //    protected DateTime EndDate { get; set; }
        //    protected bool ActiveRecord { get; set; }
        //    protected string JobTitle { get; set; }
        //    protected string InternalCompanyID { get; set; } //if emp has ID given to them by company provided to us
        //    protected string SupervisorName { get; set; }
        //    protected string SupPhone { get; set; } //with extension
        //    protected string SupEmail { get; set; } //name and domain
        //    protected string EmployeeFirst { get; set; }
        //    protected string EmployeeLast { get; set; }
        //    protected string EmployeeMiddleInitial { get; set; }
        //    protected DateTime DateOfBirth { get; set; }
        //    protected string StreetAddress1 { get; set; }
        //    protected string StreetAddress2 { get; set; }
        //    protected string City { get; set; }
        //    protected string State { get; set; }
        //    protected string Zip1 { get; set; }
        //    protected string Zip2 { get; set; }
        //    protected string EmpEmail { get; set; } //name and domain
        //    protected string ApplicatorTeam { get; set; } //name and domain
        //    protected DateTime DateCreated { get; set; }
        //    protected string CreatedByUser { get; set; } //with extension
        //    protected DateTime DateModified { get; set; } //with extension
        //    protected string ModifiedByUser { get; set; } //with extension
        //} //class EmployeeItem

        private DataTable PullEmps(SqlConnection con, DataTable Emps)
        {
            string query = "SELECT  e.[EmployeeID],e.[FirstName],e.[LastName], e.[StreetAddress1],c.CompanyID,c.CompanyName,c.CompanyBranchName " +
                            "FROM [dbo].[Employee] e " +
                            "join CompanyEmployee ce on ce.EmployeeID = e.EmployeeID " +
                            "join Company c on c.CompanyID = ce.CompanyID " +
                            "order by e.[FirstName] asc, e.[LastName] asc";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = Emps.NewRow();

                    tableRow["EmployeeID"] = Convert.ToInt16(readMe["EmployeeID"]);
                    tableRow["FirstName"] =readMe["FirstName"].ToString();
                    tableRow["LastName"] = readMe["LastName"].ToString();
                    tableRow["StreetAddress1"] = readMe["StreetAddress1"].ToString();
                    tableRow["CompanyID"] = Convert.ToInt16(readMe["CompanyID"]);
                    tableRow["CompanyName"] = readMe["CompanyName"].ToString();
                    tableRow["CompanyBranchName"] = readMe["CompanyBranchName"].ToString();

                    Emps.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }

            Session["Emps"] = Emps;
            return Emps;
        }

        private DataTable PullComps(SqlConnection con, DataTable Comps)
        {


            string query = "SELECT   "+
                            "        c.CompanyID " +
                            "       , c.CompanyName " +
                            "       , c.CompanyBranchName " +
                            "FROM Company c " +
                            "order by c.CompanyName asc ";

            SqlCommand selectCmd = new SqlCommand(query, con);
            if (con.State == ConnectionState.Closed) { con.Open(); }

            using (SqlDataReader readMe = selectCmd.ExecuteReader())
            {
                while (readMe.Read())
                {
                    DataRow tableRow = Comps.NewRow();

                    tableRow["CompanyID"] = Convert.ToInt16(readMe["CompanyID"]);
                    tableRow["CompanyName"] = readMe["CompanyName"].ToString();
                    tableRow["CompanyBranchName"] = readMe["CompanyBranchName"].ToString();

                    Comps.Rows.Add(tableRow);
                }
            }
            if (con.State == ConnectionState.Open) { con.Close(); }
            Session["Comps"] = Comps;
            return Comps;
        }
        protected void CompGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CompGrid.PageIndex = e.NewPageIndex;
            CompGrid.DataBind();
        }

        protected void EmpGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            EmpGrid.PageIndex = e.NewPageIndex;
            EmpGrid.DataBind();
        }
    }

}