﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace FertCertWebApp
{
    public partial class AddNew : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con;
            if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
            else { con = new SqlConnection(SiteMaster.connectionStr); } 

            if (!Page.IsPostBack)
            {
                List<SiteMaster.DropDownItem> companies = SiteMaster.FillCompanyDropDrown(con);
                CompanyDropList.Items.Clear();
                CompanyDropList.DataSource = companies;
                CompanyDropList.DataTextField = "companyName";
                CompanyDropList.DataValueField = "companyID";
                CompanyDropList.DataBind();
            }
           
        }//Page_Load
        public class DropDownItem
        {
            public int companyID { get; set; }
            public string companyName { get; set; }
            //public string branchName { get; set; }
        }//class DropDownItem

        protected void SaveNewEmpButton_Click(object sender, EventArgs e)
        {
            // TEST FOR EMPTY
            bool first, last, start, email;
            first = String.IsNullOrWhiteSpace(FirstNameBox.Text);
            last = String.IsNullOrWhiteSpace(LastNameBox.Text);
            start = String.IsNullOrWhiteSpace(EmpStartDateBox.Text);
            email = String.IsNullOrWhiteSpace(EmpEmailBox.Text);

            if (first || last || start || email)
            {
                string spanB = "<li><span class=\"important\"><h4 class=\"text-danger\">";
                string spanE = "</h4></span></li>";
                string warningLabel = "<h4>There are values missing from your input. Please enter: </h4><ul>";
                if (first) warningLabel += spanB + "First Name" + spanE;
                if (last) warningLabel += spanB + "Last Name" + spanE;
                if (start) warningLabel += spanB + "Employee Start Date" + spanE;
                if (email) warningLabel += spanB + "Email Address" + spanE;

                warningLabel += "</ul><h4>Before saving the new employee to the database!</h4>";
                EmpSaveWarning.Text = warningLabel;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompWarning');", true);
                return;
            }

            //PREP FOR SAVING TO DB
            string firstN = TextCleaner(FirstNameBox.Text, true, true, true, true, false, false);
            string middleN = TextCleaner(MiddleInitBox.Text, true, true, true, true, false, false);
            string lastN = TextCleaner(LastNameBox.Text, true, true, true, true, false, false);
            string birthdate = BirthBox.Text;
            string[] emailN = EmailCleaner(EmpEmailBox.Text);
            string company = CompanyDropList.Text;
            string companyID = CompanyDropList.SelectedValue.ToString();
            string phone = NumberCleaner(PhoneBox.Text);
            string phoneExt = NumberCleaner(PhoneExtBox.Text);
            string addr1 = TextCleaner(Address1Box.Text, true, true, false, false, false, false);
            string addr2 = TextCleaner(Address2Box.Text, true, true, false, false, false, false);
            string city = TextCleaner(CityBox.Text, true, true, false, true, false, false);
            string state = TextCleaner(StateBox.Text, true, true, true, false, false, true);
            string zip1 = NumberCleaner(Zip1Box.Text);
            string zip2 = NumberCleaner(Zip2Box.Text);
            string jobTitle = TextCleaner(EmpJobTitleBox.Text, true, true, true, true, false, false);
            string compEmpID = TextCleaner(EmpCompanyIDBox.Text, true, true, false, false, false, false);
            string empStartDate = EmpStartDateBox.Text;
            string supName = TextCleaner(SupNameBox.Text, true, true, true, true, false, false);
            string[] supEmail = EmailCleaner(SupEmailBox.Text);
            string supPhone = NumberCleaner(SupPhoneBox.Text);
            string supPhoneExt = NumberCleaner(SupPhoneExtBox.Text);

            string logUser = HttpContext.Current.User.Identity.Name.ToString();
            string today = DateTime.Today.ToShortDateString();

            //error bools
            bool rollback = false;
            int newEmpDatabaseID = -1;
            int newCompEmpDatabaseID = -1;
            int rowsAffected = 0;

            try
            {
                SqlConnection con;
                if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
                else { con = new SqlConnection(SiteMaster.connectionStr); }

                SqlTransaction transaction;

                string empInsText =
                    "INSERT INTO [dbo].[Employee] (" +
                        " [FirstName],[LastName],[MiddleInitial],[EmpPhone],[EmpPhoneExt],[EmpEmailName],[EmpEmailDomain],[DateOfBirth]" +
                        ",[StreetAddress1],[StreetAddress2],[City],[AddressState],[Zip1],[Zip2],[EmpCompanyID]" +
                    ") VALUES (" +
                        "@FirstName, @LastName, @MiddleInitial, @EmpPhone, @EmpPhoneExt, @EmpEmailName, @EmpEmailDomain, @DateOfBirth, " +
                        "@StreetAddress1, @StreetAddress2, @City, @AddressState, @Zip1, @Zip2, @CompEmpID" +
                    ");";

                SqlCommand insertEmpCmd = new SqlCommand(empInsText, con);

                SqlParameter[] insertEmpParams = {
                    new SqlParameter() { ParameterName = "@FirstName",          SqlDbType = SqlDbType.VarChar,  Value = firstN      },
                    new SqlParameter() { ParameterName = "@LastName",           SqlDbType = SqlDbType.VarChar,  Value = lastN       },
                    new SqlParameter() { ParameterName = "@MiddleInitial",      SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(middleN))? middleN : (object)DBNull.Value     },
                    new SqlParameter() { ParameterName = "@EmpPhone",           SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(phone))? phone : (object)DBNull.Value       },
                    new SqlParameter() { ParameterName = "@EmpPhoneExt",        SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(phoneExt))? phoneExt : (object)DBNull.Value    },
                    new SqlParameter() { ParameterName = "@EmpEmailName",       SqlDbType = SqlDbType.VarChar,  Value = emailN[0]   },
                    new SqlParameter() { ParameterName = "@EmpEmailDomain",     SqlDbType = SqlDbType.VarChar,  Value = emailN[1]   },
                    new SqlParameter() { ParameterName = "@DateOfBirth",        SqlDbType = SqlDbType.VarChar,  Value = birthdate },
                    new SqlParameter() { ParameterName = "@StreetAddress1",     SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(addr1))? addr1: (object)DBNull.Value       },
                    new SqlParameter() { ParameterName = "@StreetAddress2",     SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(addr2))? addr2 : (object)DBNull.Value       },
                    new SqlParameter() { ParameterName = "@City",               SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(city))? city : (object)DBNull.Value        },
                    new SqlParameter() { ParameterName = "@AddressState",       SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(state))? state : (object)DBNull.Value       },
                    new SqlParameter() { ParameterName = "@Zip1",               SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(zip1))? zip1 : (object)DBNull.Value        },
                    new SqlParameter() { ParameterName = "@Zip2",               SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(zip2))? zip2 : (object)DBNull.Value        },
                    new SqlParameter() { ParameterName = "@CompEmpID",          SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(compEmpID))? compEmpID : (object)DBNull.Value  }
                };
                insertEmpCmd.Parameters.AddRange(insertEmpParams);

                using (con)
                {
                    if (con.State == ConnectionState.Closed) con.Open();
                    transaction = con.BeginTransaction("insertEmployee");
                    try
                    {
                        insertEmpCmd.Transaction = transaction;
                        rowsAffected = insertEmpCmd.ExecuteNonQuery();
                        if (rowsAffected == 1)
                        {
                            rowsAffected = 0;
                            string selectText = "SELECT TOP 1 [EmployeeID] FROM [dbo].[Employee] WHERE [FirstName] = @FirstName and [LastName] = @LastName  ORDER BY [EmployeeID] DESC"; //and [CreatedByUser] = @CreatedByUser and [DateCreated] = @DateCreated
                            SqlCommand selectCmd = new SqlCommand(selectText, con);
                            selectCmd.Transaction = transaction;

                            SqlParameter[] selectEmpParams = {
                                new SqlParameter() { ParameterName = "@FirstName",          SqlDbType = SqlDbType.VarChar,  Value = firstN      },
                                new SqlParameter() { ParameterName = "@LastName",           SqlDbType = SqlDbType.VarChar,  Value = lastN       }
                            };
                            selectCmd.Parameters.AddRange(selectEmpParams);

                            using (SqlDataReader readMe = selectCmd.ExecuteReader())
                            {
                                while (readMe.Read())
                                {
                                    string i = readMe["EmployeeID"].ToString();
                                    newEmpDatabaseID = Convert.ToInt32(i);
                                }
                            }

                            if (newEmpDatabaseID > 0)
                            {
                                string empCompInsText =
                                    "INSERT INTO [dbo].[CompanyEmployee] (" +
                                        "[CompanyID],[EmployeeID],[EmployeeStartDate],[EmployeeEndDate],[ActiveRecord],[EmployeeJobTitle]," +
                                        "[EmployeeIDatCompany],[SupervisorName],[SupPhone],[SupPhoneExt],[SupEmailName],[SupEmailDomain]" +
                                    ") VALUES (" +
                                        "@CompanyID,@EmployeeID,@EmployeeStartDate,@EmployeeEndDate,@ActiveRecord,@EmployeeJobTitle," +
                                        "@EmployeeIDatCompany,@SupervisorName,@SupPhone,@SupPhoneExt,@SupEmailName,@SupEmailDomain" +
                                    ");";

                                SqlCommand insertCompCmd = new SqlCommand()
                                {
                                    Connection = con,
                                    CommandText = empCompInsText,
                                    Transaction = transaction
                                };

                                SqlParameter[] insertCompParams = {
                                    new SqlParameter() { ParameterName = "@CompanyID",          SqlDbType = SqlDbType.Int,      Value = Convert.ToInt32(companyID)                                                     },
                                    new SqlParameter() { ParameterName = "@EmployeeID",         SqlDbType = SqlDbType.Int,      Value = newEmpDatabaseID                                                               },
                                    new SqlParameter() { ParameterName = "@EmployeeStartDate",  SqlDbType = SqlDbType.Date,     Value = empStartDate                                                                   },
                                    new SqlParameter() { ParameterName = "@EmployeeEndDate",    SqlDbType = SqlDbType.Date,     Value = (object)DBNull.Value                                                           },
                                    new SqlParameter() { ParameterName = "@ActiveRecord",       SqlDbType = SqlDbType.Bit,      Value = 1                                                                              },
                                    new SqlParameter() { ParameterName = "@EmployeeJobTitle",   SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(jobTitle))? jobTitle         : (object)DBNull.Value     },
                                    new SqlParameter() { ParameterName = "@EmployeeIDatCompany",  SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(compEmpID))? compEmpID       : (object)DBNull.Value     },
                                    new SqlParameter() { ParameterName = "@SupervisorName",     SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(supName))? supName           : (object)DBNull.Value     },
                                    new SqlParameter() { ParameterName = "@SupPhone",           SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(supPhone))? supPhone         : (object)DBNull.Value     },
                                    new SqlParameter() { ParameterName = "@SupPhoneExt",        SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(supPhoneExt))? supPhoneExt   : (object)DBNull.Value     },
                                    new SqlParameter() { ParameterName = "@SupEmailName",       SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(supEmail[0]))? supEmail[0]   : (object)DBNull.Value     },
                                    new SqlParameter() { ParameterName = "@SupEmailDomain",     SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(supEmail[1]))? supEmail[1]   : (object)DBNull.Value     }
                                };
                                insertCompCmd.Parameters.AddRange(insertCompParams);

                                rowsAffected = insertCompCmd.ExecuteNonQuery();
                                if (rowsAffected == 1)
                                {
                                    rowsAffected = 0;
                                    string selectText2 = "SELECT TOP 1 [CompanyEmployeeID] FROM [dbo].[CompanyEmployee] WHERE [CompanyID] = @CompanyID and [EmployeeID] = @EmployeeID and [EmployeeStartDate] = @EmployeeStartDate  ORDER BY [CompanyEmployeeID] DESC"; //and [CreatedByUser] = @CreatedByUser and [DateCreated] = @DateCreated
                                    SqlCommand selectCmd2 = new SqlCommand(selectText2, con);
                                    selectCmd2.Transaction = transaction;

                                    SqlParameter[] selectParams = {
                                        new SqlParameter() { ParameterName = "@CompanyID",          SqlDbType = SqlDbType.Int,      Value = companyID           },
                                        new SqlParameter() { ParameterName = "@EmployeeID",         SqlDbType = SqlDbType.Int,      Value = newEmpDatabaseID    },
                                        new SqlParameter() { ParameterName = "@EmployeeStartDate",  SqlDbType = SqlDbType.Date,     Value = empStartDate        }
                                    };
                                    selectCmd2.Parameters.AddRange(selectParams);

                                    using (SqlDataReader readMe = selectCmd2.ExecuteReader())
                                    {
                                        while (readMe.Read())
                                        {
                                            string i = readMe["CompanyEmployeeID"].ToString();
                                            newCompEmpDatabaseID = Convert.ToInt32(i);
                                        }
                                    }

                                    if (newCompEmpDatabaseID > 0)
                                    {
                                        string insertHistText =
                                            "INSERT INTO [dbo].[CreateHistory] (" +
                                                "[TableName],[RowID],[DateCreated],[CreatedByUser]" +
                                            ") VALUES (" +
                                                "@Table, @RowID, @Date, @User" +
                                            ")";
                                        SqlCommand insertHistCmd = new SqlCommand()
                                        {
                                            Connection = con,
                                            CommandText = insertHistText,
                                            Transaction = transaction
                                        };

                                        SqlParameter[] insertHistParams = {
                                            new SqlParameter() { ParameterName = "@Table",      SqlDbType = SqlDbType.VarChar,  Value = "Employee"          },
                                            new SqlParameter() { ParameterName = "@RowID",      SqlDbType = SqlDbType.Int,      Value = newEmpDatabaseID    },
                                            new SqlParameter() { ParameterName = "@Date",       SqlDbType = SqlDbType.Date,     Value = today               },
                                            new SqlParameter() { ParameterName = "@User",       SqlDbType = SqlDbType.VarChar,  Value = logUser             }
                                        };
                                        insertHistCmd.Parameters.AddRange(insertHistParams);
                                        rowsAffected = insertHistCmd.ExecuteNonQuery();

                                        insertHistCmd.Parameters.Clear();
                                        insertHistParams = new SqlParameter[] {
                                            new SqlParameter() { ParameterName = "@Table",      SqlDbType = SqlDbType.VarChar,  Value = "CompanyEmployee"       },
                                            new SqlParameter() { ParameterName = "@RowID",      SqlDbType = SqlDbType.Int,      Value = newCompEmpDatabaseID    },
                                            new SqlParameter() { ParameterName = "@Date",       SqlDbType = SqlDbType.Date,     Value = today                   },
                                            new SqlParameter() { ParameterName = "@User",       SqlDbType = SqlDbType.VarChar,  Value = logUser                 }
                                        };
                                        insertHistCmd.Parameters.AddRange(insertHistParams);
                                        rowsAffected += insertHistCmd.ExecuteNonQuery();

                                        if (rowsAffected == 2) transaction.Commit();
                                        else
                                        {
                                            transaction.Rollback(); //Hist table unable to be inserted
                                            rollback = true;

                                            string amount = rowsAffected > 2 ? " TOO MANY (" + rowsAffected.ToString() + ") " : (rowsAffected==1? "NOT ENOUGH (only 1) " : "NO ");
                                            CompErrorLabel.Text = "There was a problem saving your changes to the database: the [CreateHistory] table was unable to be properly updated.";
                                            CompHintLabel.Text = "The update process of the Create History table should insert TWO rows into your table. This error indicates that there were" 
                                                + amount + "rows inserted into the Create History database table instead, causing a rollback of all changes.";
                                        }
                                    }
                                    else
                                    {
                                        transaction.Rollback(); //newCompEmpDatabaseID unable to be pulled properly
                                        rollback = true;

                                        CompErrorLabel.Text = "There was a problem saving your changes to the database: updates to the [EmployeeCompany] table were unable to be properly verified.";
                                        CompHintLabel.Text = "The update process of the [EmployeeCompany] table should insert one row. "
                                            + "This error indicates that there may have been a successful attempt, however, post-insertion checks have failed causing a rollback of all changes.";
                                    }
                                }
                                else
                                {
                                    transaction.Rollback(); //CompanyEmployee table unable to be inserted
                                    rollback = true;

                                    string amount = rowsAffected > 1 ? " TOO MANY (" + rowsAffected.ToString() + ") " : "NO ";
                                    CompErrorLabel.Text = "There was a problem saving your changes to the database: the [EmployeeCompany] table was unable to be properly updated.";
                                    CompHintLabel.Text = "The update process of the [EmployeeCompany] table should insert ONE row into your table. This error indicates that there were"
                                        + amount + "rows inserted into the [EmployeeCompany] database table instead, causing a rollback of all changes.";
                                }
                            }
                            else
                            {
                                transaction.Rollback(); //newEmpDatabaseID unable to be pulled properly
                                rollback = true;

                                CompErrorLabel.Text = "There was a problem saving your changes to the database: updates to the [Employee] table were unable to be properly verified.";
                                CompHintLabel.Text = "The update process of the [Employee] table should insert one row. "
                                    + "This error indicates that there may have been a successful attempt, however, post-insertion checks have failed causing a rollback of all changes.";
                            }
                        }
                        else
                        {
                            transaction.Rollback(); //Employee table unable to be inserted
                            rollback = true;

                            string amount = rowsAffected > 1 ? " TOO MANY (" + rowsAffected.ToString() + ") " : "NO ";
                            CompErrorLabel.Text = "There was a problem saving your changes to the database: the [Employee] table was unable to be properly updated.";
                            CompHintLabel.Text = "The update process of the [Employee] table should insert ONE row into your table. This error indicates that there were"
                                + amount + "rows inserted into the [Employee] database table instead, causing a rollback of all changes.";
                        }
                    }
                    catch (Exception exTRANS)
                    {
                        try
                        {
                            transaction.Rollback();
                            rollback = true;
                        }
                        finally
                        {
                            Console.WriteLine("Rollback Exception Type: {0}", exTRANS.GetType());
                            Console.WriteLine("  Message: {0}", exTRANS.Message);
                        }
                    }
                }
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception exOUTER)
            {
                Console.WriteLine("Rollback Exception Type: {0}", exOUTER.GetType());
                Console.WriteLine("  Message: {0}", exOUTER.Message);
                rollback = true;
            }

            if (rollback)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompError');", true);
            }
            else
            {
                CreateEmpSuccessPop(firstN, middleN, lastN, birthdate, emailN, company, companyID, phone, phoneExt, addr1, addr2, city, state, zip1, zip2, jobTitle, compEmpID, empStartDate, supName, supEmail, supPhone, supPhoneExt, newEmpDatabaseID.ToString(), newCompEmpDatabaseID.ToString(), logUser, today);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalEmpSuccess');", true);
            }
        } //SaveNewEmpButton_Click

        /// <summary>
        ///     Fills popup when new employee successfully added to db, all parameters self explanatory.
        /// </summary>
        protected void CreateEmpSuccessPop(string firstN, string middleN, string lastN, string birthdate, string[] emailN, string company, string companyID, string phone, string phoneExt, string addr1, string addr2, string city, string state, string zip1, string zip2, string jobTitle, string compEmpID, string empStartDate, string supName, string[] supEmail, string supPhone, string supPhoneExt, string empID, string empCompID, string logUser, string today)
        {
            string blank = "<span class=\"grey-out\">[none listed]</span>";
            //SuccessLabel_CompName.Text = company;
            //SuccessLabel_BranchName.Text = branchN;

            //emp info
            string name = firstN + " " + (!string.IsNullOrEmpty(middleN) ? middleN : "") + " " + lastN;
            SuccessLabel_EmpName.Text = name;
            SuccessLabel_EmpDOB.Text = birthdate;
            SuccessLabel_EmpEmail.Text = emailN[0] + " " + emailN[1];
            SuccessLabel_EmpComp.Text = company; //branch?????
            string empPhone = !string.IsNullOrEmpty(phone) ? phone + (!string.IsNullOrEmpty(phoneExt) ? ", ext: " + phoneExt : "") : "Ext " + phoneExt;
            SuccessLabel_EmpPhone.Text = !string.IsNullOrEmpty(empPhone) ? empPhone : blank;


            //address
            string zip = !string.IsNullOrEmpty(zip1) ? zip1 + (!string.IsNullOrEmpty(zip2) ? "-" + zip2 : "") : (!string.IsNullOrEmpty(zip2) ? zip2 : "");
            string citystate = !string.IsNullOrEmpty(city) ? city + (!string.IsNullOrEmpty(state) ? ", " + state : "") : (!string.IsNullOrEmpty(state) ? state : "");
            citystate = !string.IsNullOrEmpty(zip) ? citystate + " " + zip : citystate;
            string addr = !string.IsNullOrEmpty(addr1) ? addr1 + (!string.IsNullOrEmpty(addr2) ? ", " + addr2 : "") : (!string.IsNullOrEmpty(addr2) ? addr2 : "");
            addr = !string.IsNullOrEmpty(citystate) ? (!string.IsNullOrEmpty(addr) ? addr + "; " + citystate : citystate) : (!string.IsNullOrEmpty(addr) ? addr : "");
            SuccessLabel_EmpAddr.Text = !string.IsNullOrEmpty(addr) ? addr : blank;

            //job info
            //SuccessLabel_EmpAddr.Text = !string.IsNullOrEmpty(jobTitle) ? jobTitle : blank;
            //SuccessLabel_EmpAddr.Text = !string.IsNullOrEmpty(empStartDate) ? empStartDate : blank;

            //log
            SuccessLabel_EmpCreateUser.Text = logUser;
            SuccessLabel_EmpCreateDate.Text = today;

            SuccessLabel_CompID.Text = empID;
            SuccessLabel_CompID.Text = empCompID;
            SuccessLabel_CompID.Text = companyID;
        }//CreateEmpSuccessPop

        /// <summary>
        /// Clean text, parameters listed in the order of operations.
        /// </summary>
        /// <param name="input">string to be cleaned</param>
        /// <param name="trimEnds">bool, true for trimming whitespace from beginning and end of string</param>
        /// <param name="dupeWhitespace">bool, true for removing duplicate whitespace from within string</param>
        /// <param name="specialChars">bool, allow for only letters, spaces, and dash</param>
        /// <param name="camelCase">bool, true for capitalize first letter</param>
        /// <param name="lowerCase">bool, true for convert to lower case (will override camelcase)</param>
        /// <param name="upperCase">bool, true for convert to upper case (will override camelcase and lowercase)</param>
        /// <returns>
        /// A clean string to be saved into the DB.
        /// </returns>
        protected string TextCleaner(string input, bool trimEnds, bool dupeWhitespace, bool specialChars, bool camelCase, bool lowerCase, bool upperCase)
        {
            string output = input;
            if (string.IsNullOrEmpty(output) || string.IsNullOrWhiteSpace(output)) return "";

            //PREP FOR REGEX
            string pattern1 = @"^[\s]+|[\s]+$";
            Regex whiteSpace = new Regex(pattern1);
            string pattern2 = @"\s+";
            Regex dupeSpace = new Regex(pattern2);
            string pattern3 = @"[^a-zA-Z -]";
            Regex onlyLetters = new Regex(pattern3);

            //Trim begining/end whitespace, replace with "" //-----------------------------
            if (trimEnds) output = whiteSpace.Replace(output, "");
            //-----------------------------------------------------------------------------


            //Trim duplicate whitespace inbetween, replace with " " //---------------------
            if (dupeWhitespace) output = dupeSpace.Replace(output, " ");
            //-----------------------------------------------------------------------------


            //Remove special characters //-------------------------------------------------
            if (specialChars) output = onlyLetters.Replace(output, "");
            //-----------------------------------------------------------------------------


            //Capitalize First letter only //----------------------------------------------
            if (camelCase)
            {
                output = output.ToLower();
                if (output.Contains(" "))
                {
                    output = dupeSpace.Replace(output, " ");
                    string[] out1 = output.Split('\x20');
                    string newOutput = "";
                    foreach (string o in out1)
                    {
                        if (o.Length == 1) newOutput += char.ToUpper(o[0]).ToString();
                        else newOutput += char.ToUpper(o[0]).ToString() + o.Substring(1);
                        newOutput += " ";
                    }
                    output = whiteSpace.Replace(newOutput, "");
                }
                else
                {
                    output = char.ToUpper(output[0]).ToString() + output.Substring(1);
                }
            }
            //-----------------------------------------------------------------------------


            //Lowercase all letter //------------------------------------------------------
            if (lowerCase) output = output.ToLower();
            //-----------------------------------------------------------------------------


            //Uppercase all letter //------------------------------------------------------
            if (upperCase) output = output.ToUpper();
            //-----------------------------------------------------------------------------

            return output;
        } //TextCleaner

        /// <summary>
        ///     Separates email address into Name and Domain
        /// </summary>
        /// <param name="input">email address to be seperated and cleaned</param>
        /// <returns>
        ///     string[] = {name, domain};
        /// </returns>
        protected string[] EmailCleaner(string input)
        {
            if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input)) return new string[] { "", "" };

            string[] email = input.Split('@');
            string name = TextCleaner(email[0], true, true, false, false, false, false);
            string domain = TextCleaner(email[1], true, true, false, false, true, false); ;
            email = new string[] { name, domain };

            return email;
        }//EmailCleaner

        /// <summary>
        ///     Removes unwanted characters from number strings
        /// </summary>
        /// <param name="input">string of numbers</param>
        /// <returns>
        ///     String of numbers ready for db
        /// </returns>
        protected string NumberCleaner(string input)
        {
            string output = input;
            if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input)) return "";

            string pattern1 = @"[^0-9]";
            Regex numbersOnly = new Regex(pattern1);
            output = numbersOnly.Replace(output, "");

            return output;
        }//NumberCleaner

        protected void testButton_Click(object sender, EventArgs e)
        {
            string date1 = "09/09/2020";
            string text1 = "";
            string char1 = "00100";
            bool bit1 = true;
            int int1 = 4567;

            int rowsAffected = 0;

            try
            {
                SqlConnection con;
                if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
                else { con = new SqlConnection(SiteMaster.connectionStr); }

                SqlParameter[] params1 = {
                        new SqlParameter() { ParameterName = "@date1",    SqlDbType = SqlDbType.Date,      Value = date1     },
                        new SqlParameter() { ParameterName = "@text1",    SqlDbType = SqlDbType.VarChar, Value = string.IsNullOrEmpty(text1)? (object)DBNull.Value : text1     },
                        new SqlParameter() { ParameterName = "@char1",    SqlDbType = SqlDbType.Char,      Value = char1     },
                        new SqlParameter() { ParameterName = "@bit1",     SqlDbType = SqlDbType.Bit,       Value = bit1      },
                        new SqlParameter() { ParameterName = "@int1",     SqlDbType = SqlDbType.Int,       Value = int1      }
                    };

                string query = "INSERT INTO [dbo].[TestAll] (Date1, Text1, Char1, Bit1, Int1) VALUES (@date1, @text1, @char1, @bit1, @int1);";
                SqlCommand insertTestCmd = new SqlCommand(query, con);
                insertTestCmd.Parameters.AddRange(params1);

                using (con)
                {
                    if (con.State == ConnectionState.Closed) con.Open();
                    rowsAffected = insertTestCmd.ExecuteNonQuery();
                }
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                Console.WriteLine(">>>>>>>>>>>>>>>>>>>>> Exception Type: {0}", ex.GetType());
                Console.WriteLine(">>>>>>>>>>>>>>>>>>>>> Message: {0}", ex.Message);
                Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }//testButton_Click

        protected void SaveNewCompButton_Click(object sender, EventArgs e)
        {
            //CreateSuccessPop("99999", "Caprice's Company", "Main Branch", "9900 Success Way", "Building #2", "Tallahassee", "FL", "33334", "9999", "Debbie", "Downer", new string[] { "DDowner", "companySuccess.com"}, "9997778888", "1234", "Walkerca", "2020-10-29");
            //CreateSuccessPop("99999", "Caprice's Company", "Main Branch", "", "", "", "", "", "", "", "", new string[] { "", "" }, "", "", "Walkerca", "2020-10-29");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompSuccess');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompError');", true);
            //return;

            // TEST FOR EMPTY
            bool comp, branch;
            comp = CompRadio_Branch.Checked ? false : String.IsNullOrWhiteSpace(CompNameBox.Text);
            branch = String.IsNullOrWhiteSpace(CompBranchBox.Text);

            string spanB = "<li><span class=\"important\"><h4 class=\"text-danger\">";
            string spanE = "</h4></span></li>";
            if (comp || branch)
            {
                string warningLabel = "<h4>There are values missing from your input. Please enter: </h4><ul>";
                if (comp) warningLabel += spanB + "Company Name" + spanE;
                if (branch) warningLabel += spanB + "Branch Name" + spanE;

                warningLabel += "</ul><h4>Before saving the new company to the database!</h4>";
                CompSaveWarning.Text = warningLabel;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompWarning');", true);
                return;
            }

            //PREP FOR SAVING TO DB
            string compN = CompRadio_Branch.Checked ? CompDropDown_forBranch.SelectedItem.Text : TextCleaner(CompNameBox.Text, true, true, false, false, false, false);
            string branchN = TextCleaner(CompBranchBox.Text, true, true, false, true, false, false);
            string addr1 = TextCleaner(CompAddrBox1.Text, true, true, false, false, false, false);
            string addr2 = TextCleaner(CompAddrBox2.Text, true, true, false, false, false, false);
            string city = TextCleaner(CompCityBox.Text, true, true, false, true, false, false);
            string state = TextCleaner(CompStateBox.Text, true, true, true, false, false, true);
            string zip1 = NumberCleaner(CompZipBox1.Text);
            string zip2 = NumberCleaner(CompZipBox2.Text);
            string priContFN = TextCleaner(CompPriContFirstBox.Text, true, true, true, true, false, false);
            string priContLN = TextCleaner(CompPriContLastBox.Text, true, true, true, true, false, false);
            string[] priContEmail = EmailCleaner(CompPriContEmailBox.Text);
            string priContPhone = NumberCleaner(CompPriContPhoneBox.Text);
            string priContPhoneExt = NumberCleaner(CompPriContPhoneExtBox.Text);
            string logUser = HttpContext.Current.User.Identity.Name.ToString();
            string today = DateTime.Today.ToShortDateString();

            int newCompDatabaseID = -1;
            bool rollback = false;

            try
            {
                SqlConnection con;
                if (SiteMaster.CurrentMode == SiteMaster.Mode.None) return;
                else { con = new SqlConnection(SiteMaster.connectionStr); }

                SqlTransaction transaction;
                int rowsAffected = 0;

                using (con)
                {
                    if (con.State == ConnectionState.Closed) con.Open();
                    transaction = con.BeginTransaction("insertCompany");
                    try
                    {
                        string preselectText = "SELECT TOP 1 [CompanyID] FROM [dbo].[Company] WHERE [CompanyName] = @CompanyName and [CompanyBranchName] = @CompanyBranchName  ORDER BY [CompanyID] DESC"; //and [CreatedByUser] = @CreatedByUser and [DateCreated] = @DateCreated
                        SqlCommand preselectCmd = new SqlCommand(preselectText, con);
                        preselectCmd.Transaction = transaction;

                        SqlParameter[] preselectCompParams = {
                                new SqlParameter() { ParameterName = "@CompanyName",          SqlDbType = SqlDbType.VarChar,  Value = compN      },
                                new SqlParameter() { ParameterName = "@CompanyBranchName",    SqlDbType = SqlDbType.VarChar,  Value = branchN    }
                        };
                        preselectCmd.Parameters.AddRange(preselectCompParams);

                        using (SqlDataReader readMe = preselectCmd.ExecuteReader())
                        {
                            while (readMe.Read())
                            {
                                string i = readMe["CompanyID"].ToString();
                                newCompDatabaseID = Convert.ToInt32(i);
                            }
                        }

                        if (newCompDatabaseID < 0)
                        {
                            string compInsText =
                                "INSERT INTO [dbo].[Company] (" +
                                    "[CompanyName],[CompanyBranchName],[StreetAddress1],[StreetAddress2],[City],[AddressState],[Zip1],[Zip2]," +
                                    "[PrimaryContactFirst],[PrimaryContactLast],[PrimaryContactPhone],[PrimaryContactPhoneExt],[PrimaryContactEmailName],[PrimaryContactEmailDomain]" +
                                ") VALUES (" +
                                    "@CompanyName, @CompanyBranchName, @StreetAddress1, @StreetAddress2, @City, @AddressState, @Zip1, @Zip2," +
                                    "@PrimaryContactFirst, @PrimaryContactLast, @PrimaryContactPhone, @PrimaryContactPhoneExt, @PrimaryContactEmailName, @PrimaryContactEmailDomain" +
                                ");";

                            SqlCommand insertCompCmd = new SqlCommand(compInsText, con);

                            SqlParameter[] insertCompParams = {
                                new SqlParameter() { ParameterName = "@CompanyName",                    SqlDbType = SqlDbType.VarChar,  Value = compN      },
                                new SqlParameter() { ParameterName = "@CompanyBranchName",              SqlDbType = SqlDbType.VarChar,  Value = branchN       },
                                new SqlParameter() { ParameterName = "@PrimaryContactFirst",            SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(priContFN))? priContFN : (object)DBNull.Value     },
                                new SqlParameter() { ParameterName = "@PrimaryContactLast",             SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(priContLN))? priContLN : (object)DBNull.Value     },
                                new SqlParameter() { ParameterName = "@PrimaryContactPhone",            SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(priContPhone))? priContPhone : (object)DBNull.Value       },
                                new SqlParameter() { ParameterName = "@PrimaryContactPhoneExt",         SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(priContPhoneExt))? priContPhoneExt : (object)DBNull.Value    },
                                new SqlParameter() { ParameterName = "@PrimaryContactEmailName",        SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(priContEmail[0]))? priContEmail[0] : (object)DBNull.Value  },
                                new SqlParameter() { ParameterName = "@PrimaryContactEmailDomain",      SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(priContEmail[1]))? priContEmail[1] : (object)DBNull.Value },
                                new SqlParameter() { ParameterName = "@StreetAddress1",                 SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(addr1))? addr1: (object)DBNull.Value       },
                                new SqlParameter() { ParameterName = "@StreetAddress2",                 SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(addr2))? addr2 : (object)DBNull.Value       },
                                new SqlParameter() { ParameterName = "@City",                           SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(city))? city : (object)DBNull.Value        },
                                new SqlParameter() { ParameterName = "@AddressState",                   SqlDbType = SqlDbType.VarChar,  Value = (!string.IsNullOrEmpty(state))? state : (object)DBNull.Value       },
                                new SqlParameter() { ParameterName = "@Zip1",                           SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(zip1))? zip1 : (object)DBNull.Value        },
                                new SqlParameter() { ParameterName = "@Zip2",                           SqlDbType = SqlDbType.Char,     Value = (!string.IsNullOrEmpty(zip2))? zip2 : (object)DBNull.Value        }
                            };
                            insertCompCmd.Parameters.AddRange(insertCompParams);

                            insertCompCmd.Transaction = transaction;
                            rowsAffected = insertCompCmd.ExecuteNonQuery();
                            if (rowsAffected == 1)
                            {
                                string selectText = "SELECT TOP 1 [CompanyID] FROM [dbo].[Company] WHERE [CompanyName] = @CompanyName and [CompanyBranchName] = @CompanyBranchName  ORDER BY [CompanyID] DESC"; //and [CreatedByUser] = @CreatedByUser and [DateCreated] = @DateCreated
                                SqlCommand selectCmd = new SqlCommand(selectText, con);
                                selectCmd.Transaction = transaction;

                                SqlParameter[] selectCompParams = {
                                    new SqlParameter() { ParameterName = "@CompanyName",          SqlDbType = SqlDbType.VarChar,  Value = compN      },
                                    new SqlParameter() { ParameterName = "@CompanyBranchName",    SqlDbType = SqlDbType.VarChar,  Value = branchN    }
                                };
                                selectCmd.Parameters.AddRange(selectCompParams);

                                using (SqlDataReader readMe = selectCmd.ExecuteReader())
                                {
                                    while (readMe.Read())
                                    {
                                        string i = readMe["CompanyID"].ToString();
                                        newCompDatabaseID = Convert.ToInt32(i);
                                    }
                                }

                                if (newCompDatabaseID != -1)
                                {
                                    string insertHistText =
                                        "INSERT INTO [dbo].[CreateHistory] (" +
                                            "[TableName],[RowID],[DateCreated],[CreatedByUser]" +
                                        ") VALUES (" +
                                            "@Table, @RowID, @Date, @User" +
                                        ")";
                                    SqlCommand insertHistCmd = new SqlCommand()
                                    {
                                        Connection = con,
                                        CommandText = insertHistText,
                                        Transaction = transaction
                                    };

                                    SqlParameter[] insertHistParams = {
                                        new SqlParameter() { ParameterName = "@Table",      SqlDbType = SqlDbType.VarChar,  Value = "Company"          },
                                        new SqlParameter() { ParameterName = "@RowID",      SqlDbType = SqlDbType.Int,      Value = newCompDatabaseID  },
                                        new SqlParameter() { ParameterName = "@Date",       SqlDbType = SqlDbType.Date,     Value = today              },
                                        new SqlParameter() { ParameterName = "@User",       SqlDbType = SqlDbType.VarChar,  Value = logUser            }
                                    };
                                    insertHistCmd.Parameters.AddRange(insertHistParams);
                                    rowsAffected = insertHistCmd.ExecuteNonQuery();

                                    if (rowsAffected == 1) transaction.Commit();
                                    else
                                    {
                                        transaction.Rollback(); //Hist table unable to be inserted
                                        rollback = true;

                                        string amount = rowsAffected > 1 ? " TOO MANY ("+rowsAffected.ToString()+") " : " NO ";
                                        CompErrorLabel.Text = "There was a problem saving your changes to the database: the [CreateHistory] table was unable to be properly updated.";
                                        CompHintLabel.Text = "The update process of the [CreateHistory] table should insert ONE row into your table. This error indicates that there were" + amount + "rows inserted into the Create History database table instead, causing a rollback of all changes.";
                                    }

                                }
                                else
                                {
                                    transaction.Rollback(); //CompanyID unable to be pulled properly
                                    rollback = true;
                                    
                                    CompErrorLabel.Text = "There was a problem saving your changes to the database: updates to the [Company] table were unable to be properly verified.";
                                    CompHintLabel.Text = "The update process of the [Company] table should insert one row. This error indicates that there may have been a successful attempt, however, post-insertion checks have failed causing a rollback of all changes.";
                                }

                            }
                            else
                            {
                                transaction.Rollback(); //Company table unable to be inserted
                                rollback = true;

                                string amount = rowsAffected > 1 ? " TOO MANY (" + rowsAffected.ToString() + ") " : " NO ";
                                CompErrorLabel.Text = "There was a problem saving your changes to the database: the [Company] table was unable to be properly updated.";
                                CompHintLabel.Text = "The update process of the [Company] table should insert ONE row into your table. This error indicates that there were" + amount + "rows inserted into the Company database table instead, causing a rollback of all changes.";
                            }
                        }
                        else
                        {
                            if (con.State == ConnectionState.Open) con.Close();

                            string warningLabel = "<h4>There is already a company with that branch name in the database. Please enter a new branch name for this company:</h4><ul>";
                            warningLabel += spanB + "Company name in use: " + compN + spanE;
                            warningLabel += spanB + "Branch name in use: " + branchN + spanE;

                            warningLabel += "</ul><h4>Please fix this issue before saving the new company/branch to the database!</h4>";
                            CompSaveWarning.Text = warningLabel;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompWarning');", true);
                            return;
                        }
                    }
                    catch (Exception exTRANS)
                    {
                        try { transaction.Rollback(); }
                        finally
                        {
                            Console.WriteLine("Rollback Exception Type: {0}", exTRANS.GetType());
                            Console.WriteLine("  Message: {0}", exTRANS.Message);
                            rollback = true;
                        }
                    }
                }
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception exOUTER)
            {
                Console.WriteLine("Rollback Exception Type: {0}", exOUTER.GetType());
                Console.WriteLine("  Message: {0}", exOUTER.Message);
                rollback = true;
            }

            if (rollback)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompError');", true);
            }
            else
            {
                CreateSuccessPop(newCompDatabaseID.ToString(), compN, branchN, addr1, addr2, city, state, zip1, zip2, priContFN, priContLN, priContEmail, priContPhone, priContPhoneExt, logUser, today);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal('modalCompSuccess');", true);
            }

        }//SaveNewCompButton_Click

        /// <summary>
        ///     Fills popup when new company+branch successfully added to db, all parameters self explanatory.
        /// </summary>
        protected void CreateSuccessPop(string compID, string compN, string branchN, string addr1, string addr2, string city, string state, string zip1, string zip2, string priContFN, string priContLN, string[] priContEmail, string priContPhone, string priContPhoneExt, string logUser, string today)
        {
            string blank = "<span class=\"grey-out\">[none listed]</span>";
            SuccessLabel_CompName.Text = compN;
            SuccessLabel_BranchName.Text = branchN;

            //address
            string zip = !string.IsNullOrEmpty(zip1) ? zip1 + (!string.IsNullOrEmpty(zip2) ? "-" + zip2 : "") : (!string.IsNullOrEmpty(zip2) ? zip2 : "");
            string citystate = !string.IsNullOrEmpty(city) ? city + (!string.IsNullOrEmpty(state) ? ", " + state : "") : (!string.IsNullOrEmpty(state) ? state : "");
            citystate = !string.IsNullOrEmpty(zip) ? citystate + " " + zip : citystate;
            string addr = !string.IsNullOrEmpty(addr1) ? addr1 + (!string.IsNullOrEmpty(addr2) ? ", " + addr2  : "") : (!string.IsNullOrEmpty(addr2) ? addr2 : "");
            addr = !string.IsNullOrEmpty(citystate) ? (!string.IsNullOrEmpty(addr)? addr + "; " + citystate : citystate) : (!string.IsNullOrEmpty(addr) ? addr  : "");
            SuccessLabel_CompAddr.Text = !string.IsNullOrEmpty(addr) ? addr : blank ;

            //contact info
            string name = !string.IsNullOrEmpty(priContFN) ? priContFN + (!string.IsNullOrEmpty(priContLN) ? " " + priContLN : "") : (!string.IsNullOrEmpty(priContLN) ? priContLN : "");
            SuccessLabel_CompPriContact.Text = !string.IsNullOrEmpty(name) ? name : blank;

            string email = priContEmail[0] + (!string.IsNullOrEmpty(priContEmail[1]) ? "@" + priContEmail[1] : "");
            SuccessLabel_CompEmail.Text = !string.IsNullOrEmpty(email) ? email : blank;

            string phone = priContPhone + (!string.IsNullOrEmpty(priContPhoneExt) ? ", ext: " + priContPhoneExt : "");
            SuccessLabel_CompPriPhone.Text = !string.IsNullOrEmpty(phone) ? phone : blank; ;

            //log
            SuccessLabel_CompCreateUser.Text = logUser;
            SuccessLabel_CompCreateDate.Text = today;

            SuccessLabel_CompID.Text = compID;
        }//CreateSuccessPop
        
        
        /* NO LONGER NECESSARY -- HANDLED WITH JS
        protected void CompRadio_CheckedChanged(string input)
        {
            if (input == "Branch")
            {
                CompNameBox.Visible = false;
                CompDropDown_forBranch.Visible = true;
                CompBranchBox.Text = "";
            }
            else
            {
                CompNameBox.Visible = true;
                CompDropDown_forBranch.Visible = false;
                CompBranchBox.Text = (input == "Comp") ? "Main Branch" : "";


            }
        }//CompRadio_CheckedChanged
        protected void CompRadio_Comp_CheckedChanged(object sender, EventArgs e)
        {
            string input = "Comp";
            CompRadio_CheckedChanged(input);
        }//CompRadio_Comp_CheckedChanged
        protected void CompRadio_CompBranch_CheckedChanged(object sender, EventArgs e)
        {
            string input = "CompBranch";
            CompRadio_CheckedChanged(input);
        }//CompRadio_CompBranch_CheckedChanged
        protected void CompRadio_Branch_CheckedChanged(object sender, EventArgs e)
        {
            string input = "Branch";
            CompRadio_CheckedChanged(input);
        }//CompRadio_Branch_CheckedChanged
    */
    }
}