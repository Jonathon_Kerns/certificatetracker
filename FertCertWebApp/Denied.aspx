﻿<%-- <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Denied.aspx.cs" Inherits="FertCertWebApp.Denied" MasterPageFile="~/Deny.Master"%>  --%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Denied.aspx.cs" Inherits="FertCertWebApp.Denied" %>  


<%-- <asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server"> --%>
    <div class="page-title d">
        <h3>* Access Denied *</h3>
        <h3>* * *</h3>
        <p>You do not have the required permissions to access these pages.</p>
        <p>If you feel you have received this message in error, please contact your system adminitrator to gain the proper credentials.</p>
        <h3>* * *</h3>
    </div>
<%-- </asp:Content>  --%>
